<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
   //打开Word文档
    poCtrl.webOpen("/DataRegionText/doc/test.doc", OpenModeType.docNormalEdit, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>最简单的打开保存Word文件</title>
</head>
<body>
    <script type="text/javascript">
        //控件中的一些常用方法都在这里调用，比如保存，打印等等
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.CustomToolbar = false;
        }
    </script>
<form id="form1">
    <div style=" width:auto; height:700px;">
        <%=poCtrl.getHtml()%>
    </div>
</form>
</body>
</html>
