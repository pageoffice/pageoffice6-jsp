<%@ page language="java" import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.*" %>
<%@ page import="java.awt.*" %>
<%
	PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

	WordDocumentWriter doc = new WordDocumentWriter();
	DataRegionWriter d1 = doc.openDataRegion("d1");
	d1.getFont().setColor(Color.BLUE);//设置数据区域文本字体颜色
	d1.getFont().setName("华文彩云");//设置数据区域文本字体样式
	d1.getFont().setSize(16);//设置数据区域文本字体大小
	d1.getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);//设置数据区域文本对齐方式

	DataRegionWriter d2 = doc.openDataRegion("d2");
	d2.getFont().setColor(Color.orange);//设置数据区域文本字体颜色
	d2.getFont().setName("黑体");//设置数据区域文本字体样式
	d2.getFont().setSize(14);//设置数据区域文本字体大小
	d2.getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);//设置数据区域文本对齐方式

	DataRegionWriter d3 = doc.openDataRegion("d3");
	d3.getFont().setColor(Color.magenta);//设置数据区域文本字体颜色
	d3.getFont().setName("华文行楷");//设置数据区域文本字体样式
	d3.getFont().setSize(12);//设置数据区域文本字体大小
	d3.getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphRight);//设置数据区域文本对齐方式

	poCtrl.setWriter(doc);
	//打开Word文档
	poCtrl.webOpen("/DataRegionText/doc/test.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <title></title>
    <link href="images/csstg.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <script type="text/javascript">
        //控件中的一些常用方法都在这里调用，比如保存，打印等等
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.CustomToolbar = false;
        }
    </script>
<div id="content">
    演示了如果使用程序控制数据区域文本的样式。
    <div id="textcontent" style="width: 1000px; height: 800px;">

        <!--**************   卓正 PageOffice组件 ************************-->
        <%=poCtrl.getHtml()%>
    </div>
</div>
</body>
</html>
