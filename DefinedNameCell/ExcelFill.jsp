<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@page import="com.zhuozhengsoft.pageoffice.excel.SheetWriter" %>
<%@page import="com.zhuozhengsoft.pageoffice.excel.WorkbookWriter" %>
<%
	PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

	//定义Workbook对象
	WorkbookWriter workBook = new WorkbookWriter();
	//定义Sheet对象，"Sheet1"是打开的Excel表单的名称
	SheetWriter sheet = workBook.openSheet("Sheet1");
	sheet.openCellByDefinedName("testA1").setValue("Tom");
	sheet.openCellByDefinedName("testB1").setValue("John");

	poCtrl.setWriter(workBook);

	//打开excel文档
	poCtrl.webOpen("doc/test.xls", OpenModeType.xlsNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>给Excel文档中定义名称的单元格赋值</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <script type="text/javascript">
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.AddCustomToolButton("保存", "Save", 1);

        }
        function Save() {
			//设置保存方法
            pageofficectrl.SaveDataPage="/DefinedNameCell/SaveData.jsp";
            //在这里写您保存前的代码
            pageofficectrl.WebSave();
            //在这里写您保存后的代码，比如判断保存结果pageofficectrl.CustomSaveResult
            alert(pageofficectrl.CustomSaveResult);
        }
    </script>
</head>
<body>
A1、B1单元格的数据是使用后台程序填充进去的，请查看ExcelFill.jsp的代码
<div style="width: 1000px; height: 800px;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
