<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.excel.SheetReader, com.zhuozhengsoft.pageoffice.excel.WorkbookReader"
         pageEncoding="utf-8" %>
<%
    WorkbookReader workBook = new WorkbookReader(request, response);
    SheetReader sheet = workBook.openSheet("Sheet1");
    String content = "";
    content += "testA1：" + sheet.openCellByDefinedName("testA1").getValue() + "\r\n";
    content += "testB1：" + sheet.openCellByDefinedName("testB1").getValue() + "\r\n";
    /**
     * 实际开发中，一般获取数据区域的值后用来和数据库进行交互，比如根据刚才获取的数据进行数据库记录的新增，更新，删除等。
     * 此处为了给用户展示获取的数据内容，通过setCustomSaveResult将获取的数据区域的值返回到前端页面给用户检查执行的结果。
     * 如果只是想返回一个保存结果，可以使用比如：setCustomSaveResult("ok")，前端可以根据这个保存结果进行下一步逻辑处理。
     */
    workBook.setCustomSaveResult(content);
    workBook.close();
%>
