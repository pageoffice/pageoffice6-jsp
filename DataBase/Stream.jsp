<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    //******************************卓正PageOffice组件的使用*******************************
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    //打开Word文件
    poCtrl.webOpen("Openstream.jsp?id=1", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title></title>
</head>
<body>
	<!-- *********************PageOffice组件客户端JS代码*************************** -->
	<script type="text/javascript">
		function Save() {
			//设置保存方法
			pageofficectrl.SaveFilePage="/DataBase/SaveFile.jsp?id=1"				
			pageofficectrl.WebSave();
			if (pageofficectrl.CustomSaveResult == "ok") {
				alert('保存成功！');
			}
		}

		function OnPageOfficeCtrlInit() {
			// PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
			pageofficectrl.AddCustomToolButton("保存", "Save", 1);
		}
	</script>

    <div style="width: auto; height: 700px;">
        <!-- *********************PageOffice组件的引用*************************** -->
        <%=poCtrl.getHtml()%>
    </div>

</body>
</html>
