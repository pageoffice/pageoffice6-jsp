﻿<%@ page language="java" import="com.zhuozhengsoft.pageoffice.OpenModeType" pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.PageOfficeCtrl" %>
<%@ page import="com.zhuozhengsoft.pageoffice.OfficeVendorType" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    poCtrl.wordCompare("doc/aaa1.doc", "doc/aaa2.doc", OpenModeType.docAdmin, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Word文档比较</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
</head>
<body>
<script language="javascript" type="text/javascript">
    function ShowFile1View() {
        pageofficectrl.word.ShowCompareView(1);
    }

    function ShowFile2View() {
        pageofficectrl.word.ShowCompareView(2);
    }

    function ShowCompareView() {
        pageofficectrl.word.ShowCompareView(0);
    }

    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
        pageofficectrl.AddCustomToolButton("显示A文档", "ShowFile1View()", 0);
        pageofficectrl.AddCustomToolButton("显示B文档", "ShowFile2View()", 0);
        pageofficectrl.AddCustomToolButton("显示比较结果", "ShowCompareView()", 0);

    }
</script>
<div style="width:1000px; height:800px;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
