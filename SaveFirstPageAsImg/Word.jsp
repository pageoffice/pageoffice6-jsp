<%@ page language="java" import="com.zhuozhengsoft.pageoffice.OpenModeType" pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.PageOfficeCtrl" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>保存Word首页为图片</title>
	<script type="text/javascript">
		function Save() {
			//设置保存方法
            pageofficectrl.SaveFilePage="/SaveFirstPageAsImg/SaveFile.jsp"
			pageofficectrl.WebSave();
		}

		function SaveFirstAsImg() {
			//设置保存方法
            pageofficectrl.SaveFilePage="/SaveFirstPageAsImg/SaveFile.jsp"
			pageofficectrl.WebSaveAsImage();
			alert('图片被保存到“SaveFirstPageAsImg/images/”目录下');
		}
		
		function OnPageOfficeCtrlInit() {
			pageofficectrl.AddCustomToolButton("保存", "Save()", 1);
			pageofficectrl.AddCustomToolButton("保存首页为图片", "SaveFirstAsImg()", 1);
		}
	</script>
</head>
<body>
<div style="width:auto; height:98vh;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
