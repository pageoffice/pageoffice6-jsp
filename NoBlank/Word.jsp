<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    //******************************卓正PageOffice组件的使用*******************************
    //设置PageOffice服务器组件
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    //打开文件
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>打开Word文档</title>
	<style>
        *{
            margin : 0;
            padding : 0;
        }
        div{
            position : absolute;
            width : 100%;
            height : 100%;
        }
        body{
            overflow: hidden;
        }
    </style>
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数

        pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
    }

</script>
<div >
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
