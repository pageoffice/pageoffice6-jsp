<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.*" %>
<%@ page import="java.awt.*" %>
<%
    WordDocumentWriter doc = new WordDocumentWriter();
    DataRegionWriter dataReg = doc.openDataRegion("PO_table");
    WordTableWriter table = dataReg.openTable(1);
    //合并table中的单元格
    table.openCellRC(1, 1).mergeTo(1, 4);
    //给合并后的单元格赋值
    table.openCellRC(1, 1).setValue("销售情况表");
    //设置单元格文本样式
    table.openCellRC(1, 1).getFont().setColor(Color.red);
    table.openCellRC(1, 1).getFont().setSize(24);
    table.openCellRC(1, 1).getFont().setName("楷体");
    table.openCellRC(1, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    poCtrl.setWriter(doc);

    //设置文档打开方式
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <title></title>
    <link href="images/csstg.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数

        pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
    }

</script>
<div id="content">
    <div id="textcontent" style="width: 1000px; height: 800px;">
        <!--**************   卓正 PageOffice组件 ************************-->
        <%=poCtrl.getHtml()%>
    </div>
</div>
</body>
</html>
