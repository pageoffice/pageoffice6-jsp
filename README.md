# pageoffice6-jsp

**当前版本：6.5.0.1**

### 一、简介

​      pageoffice6-jsp项目演示了在JSP下如何使用PageOffice  V6.0产品，此项目演示了PageOffice产品近90%的功能，是一个demo项目。

### 二、项目环境要求

​     jdk1.8及以上版本，tomcat8.0及以上版本

### 三、项目运行步骤

1. 使用git clone（推荐）或者下载此项目压损包并解压缩。
2. 拷贝pageoffice6-jsp文件夹到Tomcat的Webapps目录下。

3. 下载PageOffice客户端程序并拷贝到此项目的WEB-INF/lib目录下

- 说明：

  PageOffice按客户端电脑操作系统不同分为两款产品。 **PageOffice Windows版** 和  **PageOffice 国产版** 。

  **PageOffice Windows版** ：支持Win7/Win8/Win10/Win11。

  **PageOffice 国产版** ：支持信创系统，支持银河麒麟V10和统信UOS，支持X86（intel、兆芯等）、ARM（飞腾、鲲鹏等）、龙芯（Loogarch64）、龙芯（MIPS）芯片架构。

  如果您的用户客户端电脑都是Windows系统，只需要下载windows版客户端。

  如果您的用户客户端电脑都是国产系统，只需要下载对应cpu芯片的国产版客户端。

  如果您的用户客户端电脑既有Windows系统又有国产系统，那么Windows安装包和国产版安装包（根据cpu芯片选择）都需要下载。

  _注意：如果您需要支持WinXP，您只能使用PageOffice5.0及早期老版本。 如果您需要支持申威架构，您目前只能使用PageOffice国产版5.0_  。

- 下载地址：

  **注意**：下面地址随着时间推移可能会失效，请下载到本地做好备份。或者直接点击 [pageoffice6-client](https://gitee.com/pageoffice/pageoffice6-client/releases) 获取pageoffice最新版本。

   Windows版客户端下载地址：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/posetup_6.5.0.1.exe

   国产版客户端下载地址：
   
   **统信UOS系统：**

    X86芯片(Intel、兆芯)：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/com.zhuozhengsoft.pageoffice_6.5.0.1_amd64_uos.deb

    ARM芯片(鲲鹏、飞腾)：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/com.zhuozhengsoft.pageoffice_6.5.0.1_arm64_uos.deb
    
    **银河麒麟V10系统：**
    
    X86芯片(Intel、兆芯)：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/com.zhuozhengsoft.pageoffice_6.5.0.1_amd64_kylin.deb
    
    ARM芯片(鲲鹏、飞腾)：https://gitee.com/pageoffice/pageoffice6-client/releases/download/v6.5.0.1/com.zhuozhengsoft.pageoffice_6.5.0.1_arm64_kylin.deb

4. 下载pageoffice的jar并拷贝到此项目的WEB-INF/lib目录下（下载地址：https://repo1.maven.org/maven2/com/zhuozhengsoft/pageoffice/6.5.0.1-javax/pageoffice-6.5.0.1-javax.jar ）。

5. 启动tomcat，浏览器访问：http://localhost:8080/pageoffice6-jsp/index.html ，即可在线打开、编辑、保存office文件 。

**注意：如果您需要电子印章功能，请下载：https://repo1.maven.org/maven2/org/xerial/sqlite-jdbc/3.32.3.2/sqlite-jdbc-3.32.3.2.jar 到此项目的“/WEB-INF/lib”文件夹下。
**

### 四、PageOffice序列号

在首次运行项目中点击 “在线打开文档” ，pageoffice 会弹出注册对话框。如果您需要使用 pageoffice windows 版，请在windows 客户端电脑上访问pageoffice页面，用windows 版序列号进行注册。如果您需要使用 pageoffice 国产版，请在国产操作系统的客户端电脑上访问pageoffice页面，用国产版序列号进行注册。

**PageOffice windows 版试用序列号：**

PageOfficeV6.0标准版试用序列号：A7VHK-HDTK-338U-NARCV

PageOfficeV6.0专业版试用序列号：6VD6L-3MJL-DASM-YD9B5

**PageOffice 国产版试用序列号：**

PageOfficeV6.0国产版试用序列号：GC-8H-693A-Z1IA-R62SJ



> 问：PageOffice浏览器窗口中如何调试？
>
> 答：PageOffice浏览器（POBrowser窗口）中，在网页空隙中右键"网页调试工具"，即可出现"PageOffice Devtools"工具窗口进行调试。

### 五、集成PageOffice到您的项目中的关键步骤

1.下载pageoffice的jar(下载地址：https://repo1.maven.org/maven2/com/zhuozhengsoft/pageoffice/6.5.0.1-javax/pageoffice-6.5.0.1-javax.jar )，并拷贝到您自己web项目的“/WEB-INF/lib”文件夹下。

2.下载客户端程序，并拷贝到自己Web项目中WEB-INF/lib目录下。

> PageOffice客户端安装程序下载地址：https://gitee.com/pageoffice/pageoffice6-client/releases

3.参考“pageoffice6-jsp/WEB-INF”文件夹中的web.xml文件，配置一下自己Web项目中的web.xml

文件，也可以直接把下面的这段配置添加到自己的web.xml 中；

```xml
<!-- PageOffice Begin -->
<servlet>
	<servlet-name>poserver</servlet-name>
	<servlet-class>com.zhuozhengsoft.pageoffice.poserver.Server</servlet-class>
</servlet>
<servlet-mapping>
	<servlet-name>poserver</servlet-name>
	<url-pattern>/poserver.zz</url-pattern>
</servlet-mapping>
<servlet-mapping>
	<servlet-name>poserver</servlet-name>
	<url-pattern>/sealsetup.exe</url-pattern>
</servlet-mapping>
<servlet-mapping>
	<servlet-name>poserver</servlet-name>
	<url-pattern>/poclient</url-pattern>
</servlet-mapping>
<servlet-mapping>
	<servlet-name>poserver</servlet-name>
	<url-pattern>/pageoffice.js</url-pattern>
</servlet-mapping>
<servlet>
	<servlet-name>adminseal</servlet-name>
	<servlet-class>com.zhuozhengsoft.pageoffice.poserver.AdminSeal</servlet-class>
</servlet>
<servlet-mapping>
	<servlet-name>adminseal</servlet-name>
	<url-pattern>/adminseal.zz</url-pattern>
</servlet-mapping>
<servlet-mapping>
	<servlet-name>adminseal</servlet-name>
	<url-pattern>/loginseal.zz</url-pattern>
</servlet-mapping>
<servlet-mapping>
	<servlet-name>adminseal</servlet-name>
	<url-pattern>/sealimage.zz</url-pattern>
</servlet-mapping>
<mime-mapping>
	<extension>mht</extension>
	<mime-type>message/rfc822</mime-type>
</mime-mapping>
<!--为了安全，强烈建议修改此密码-->
<context-param>
    <param-name>adminseal-password</param-name>
    <param-value>111111</param-value>
</context-param>
<!-- PageOffice End -->
```
3. 在需要在线编辑的 JSP 页面头部添加：

  `<%@ page language="java" import="com.zhuozhengsoft.pageoffice.*" %>`

4. 对 PageOffice 编程控制：

```java
<%
  PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request); 
  poCtrl1.setSaveFilePage("savefile.jsp");//如要保存文件，此行必须
  //打开文件
  poCtrl1.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张三");
%>
```

5. 在 html 代码的 body 中需要出现 office 界面的位置插入下面的代码：

   ```
   <%=poCtrl1.getHtml()%>
   ```

6. 调用 javascript 方法“POBrowser.openWindow”的页面一定要引用下面的 js 文件：

   ```
   <script type="text/javascript" src="/pageoffice.js"></script>
   ```

> 【注意】：pageoffice.js 文件的位置在第 2 步配置web.xml 的时候已经设置好了，
>
> 所以直接引用这个 js 即可，无需拷贝 pageoffice.js 文件到自己的Web项目目录下。


### 六、电子印章功能说明

如果您用到电子印章功能，请注意以下事项：

1.拷贝“pageoffice6-jsp/WEB-INF/lib”文件夹中的 poseal.db 和下载sqlite-jdbc-3.32.3.2.jar（https://repo1.maven.org/maven2/org/xerial/sqlite-jdbc/3.32.3.2/sqlite-jdbc-3.32.3.2.jar ）到您自己Web项目中WEB-INF/lib目录下；

2.您部署网站时，只需修改web.xml中的以下内容，其他PageOffice节点内容都无需更改。

```xml
<context-param>
  <param-name>adminseal-password</param-name>
  <!--这里修改成您的印章简易管理页的管理员登录密码，为了安全，强烈建议修改!!!-->
  <param-value>111111</param-value>
</context-param>
```

3.请参考当前项目的一、15、演示加盖印章和签字功能（以Word为例）http://localhost:8080/pageoffice6-jsp/InsertSeal/index.jsp  示例代码进行盖章功能调用。

### 七、联系我们

卓正官网：[https://www.zhuozhengsoft.com](https://gitee.com/link?target=http%3A%2F%2Fwww.zhuozhengsoft.com)

PageOffice开发者中心：[https://www.pageoffice.cn/](https://gitee.com/link?target=https%3A%2F%2Fwww.pageoffice.cn%2F)

视频演示地址：

- Windows版：[https://www.bilibili.com/video/BV1M34y1A7qL](https://gitee.com/link?target=https%3A%2F%2Fwww.bilibili.com%2Fvideo%2FBV1M34y1A7qL)
- 国产版：[https://www.bilibili.com/video/BV1eM4m167Lc/](https://gitee.com/link?target=https%3A%2F%2Fwww.bilibili.com%2Fvideo%2FBV1eM4m167Lc%2F)

联系电话：400-6600-770

QQ: 800038353
