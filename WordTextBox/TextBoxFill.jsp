<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,com.zhuozhengsoft.pageoffice.word.WordDocumentWriter"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    WordDocumentWriter doc = new WordDocumentWriter();
    doc.openDataRegion("PO_company").setValue("北京幻想科技有限公司");
    doc.openDataRegion("PO_logo").setValue("[image]/WordTextBox/doc/logo.gif[/image]");
    doc.openDataRegion("PO_dr1").setValue("左边的文本:xxxx");

    poCtrl.setWriter(doc);

    //打开Word文件
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>给Word中的文本框赋值</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
</head>
<body>
    <script type="text/javascript">
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.CustomToolbar = false;
        }
    </script>
	<div style="width: 1000px; height: 700px;">
		<%=poCtrl.getHtml()%>
	</div>
</body>
</html>
