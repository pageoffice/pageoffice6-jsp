<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,javax.servlet.*,javax.servlet.http.*"
         pageEncoding="utf-8" %>
<%
    String fileName = "zhengshi.doc"; //正式发文的文件

    //*****************************卓正PageOffice组件的使用****************************
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    poCtrl.webOpen("doc/" + fileName, OpenModeType.docReadOnly, "张三");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="images/csstg.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<form id="form2">
    <div id="header">
        <div style="float: left; margin-left: 20px;">
            <img src="images/logo.jpg" height="30"/></div>
        <ul>
            <li><a target="_blank" href="http://www.zhuozhengsoft.com">卓正网站</a></li>
            <li><a target="_blank" href="http://www.zhuozhengsoft.com/poask/index.asp">客户问吧</a></li>
            <li><a href="#">在线帮助</a></li>
            <li><a target="_blank" href="http://www.zhuozhengsoft.com/about/about/">联系我们</a></li>
        </ul>
    </div>
    <div id="content">
        <div id="textcontent" style="width: 1000px; height: 800px;">
            <div class="flow4">
                <a href="#" onclick="window.external.close();">
                    <img alt="返回" src="images/return.gif" border="0"/>文件列表</a> <span style="width: 100px;">
                    </span><strong>文档主题：</strong> <span style="color: Red;">测试文件</span>

            </div>
            <!--**************   卓正 PageOffice组件 ************************-->
            <script language="javascript">
                function OnPageOfficeCtrlInit() {
                    // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
                    pageofficectrl.AddCustomToolButton("另存到本地", "SaveAs()", 5);
                    pageofficectrl.AddCustomToolButton("页面设置", "PrintSet()", 0);
                    pageofficectrl.AddCustomToolButton("打印", "PrintFile", 6);
                    pageofficectrl.AddCustomToolButton("全屏/还原", "IsFullScreen()", 4);
                    pageofficectrl.OfficeToolbars = false;
                }
                function SaveAs() {
                    pageofficectrl.ShowDialog(3);
                }

                function PrintSet() {
                    pageofficectrl.ShowDialog(5);
                }

                function PrintFile() {
                    pageofficectrl.ShowDialog(4);
                }

                function IsFullScreen() {
                    pageofficectrl.FullScreen = !pageofficectrl.FullScreen;
                }
            </script>
            <%=poCtrl.getHtml()%>
        </div>
    </div>
    <div id="footer">
        <hr width="1000"/>
        <div>
            Copyright (c) 2019 北京卓正志远软件有限公司
        </div>
    </div>
</form>
</body>
</html>
