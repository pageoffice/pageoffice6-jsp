<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,javax.servlet.*"
         pageEncoding="utf-8" %>
<%
    String fileName = "test.doc"; // 正文内容文件：test.doc

    //***************************卓正PageOffice组件的使用********************************
    PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);

    poCtrl1.webOpen("doc/" + fileName, OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <title></title>
    <link href="images/csstg.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<form id="form2">
    <div id="header">
        <div style="float: left; margin-left: 20px;">
            <img src="images/logo.jpg" height="30"/></div>
        <ul>
            <li><a target="_blank" href="https://www.zhuozhengsoft.com">卓正网站</a></li>
            <li><a target="_blank" href="https://www.zhuozhengsoft.com/poask/index.asp">客户问吧</a></li>
            <li><a href="#">在线帮助</a></li>
            <li><a target="_blank" href="https://www.zhuozhengsoft.com/about/about/">联系我们</a></li>
        </ul>
    </div>
    <div id="content">
        <div id="textcontent" style="width: 1000px; height: 800px;">
            <div class="flow4">
                <a href="#" onclick="window.external.close();"> <img alt="返回" src="images/return.gif"
                                                                     border="0"/>文件列表</a>
                <span style="width: 100px;"> </span><strong>文档主题：</strong>
                <span style="color: Red;">测试文件</span>
            </div>
            <script type="text/javascript">
                function OnPageOfficeCtrlInit() {
                    // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
                    pageofficectrl.AddCustomToolButton("保存", "Save", 1);
                    pageofficectrl.AddCustomToolButton("全屏/还原", "IsFullScreen", 4);
                }
                function Save() {
					//设置保存方法
					pageofficectrl.SaveFilePage="/TaoHong/savefile.jsp?fileName=test.doc"
                    pageofficectrl.WebSave();
                }

                //全屏/还原
                function IsFullScreen() {
                    pageofficectrl.FullScreen = !pageofficectrl.FullScreen;
                }

            </script>
            <!--**************   卓正 PageOffice组件 ************************-->
            <%=poCtrl1.getHtml()%>
        </div>
    </div>
    <div id="footer">
        <hr width="1000"/>
        <div>
            Copyright (c) 2019 北京卓正志远软件有限公司
        </div>
    </div>
</form>
</body>
</html>
