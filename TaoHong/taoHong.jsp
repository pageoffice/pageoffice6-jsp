﻿<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,com.zhuozhengsoft.pageoffice.word.*,javax.servlet.*,javax.servlet.http.*"
         pageEncoding="utf-8" %>
<%
    String fileName = "";
    String mbName = request.getParameter("mb");
    //***************************卓正PageOffice组件的使用********************************
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    poCtrl.setCustomToolbar(false);

    if (mbName != null && mbName.trim() != "") {
        //选择模板后执行套红
        fileName = request.getParameter("mb");

        // 填充数据和正文内容到红头文件
        WordDocumentWriter doc = new WordDocumentWriter();
        DataRegionWriter copies = doc.openDataRegion("PO_Copies");
        copies.setValue("6");
        DataRegionWriter docNum = doc.openDataRegion("PO_DocNum");
        docNum.setValue("001");
        DataRegionWriter issueDate = doc.openDataRegion("PO_IssueDate");
        issueDate.setValue("2013-5-30");
        DataRegionWriter issueDept = doc.openDataRegion("PO_IssueDept");
        issueDept.setValue("开发部");
        DataRegionWriter sTextS = doc.openDataRegion("PO_STextS");
        sTextS.setValue("[word]/TaoHong/doc/test.doc[/word]");
        DataRegionWriter sTitle = doc.openDataRegion("PO_sTitle");
        sTitle.setValue("北京某公司文件");
        DataRegionWriter topicWords = doc.openDataRegion("PO_TopicWords");
        topicWords.setValue("Pageoffice、 套红");
        poCtrl.setWriter(doc);
		//将套红后的文件名称命名为zhengshi.doc
		String fileName1="zhengshi.doc";
		poCtrl.setSaveFilePage("savefile.jsp?fileName="+fileName1);

    } else {
        //首次加载时，加载正文内容：test.doc
        fileName = "test.doc";
		poCtrl.setSaveFilePage("savefile.jsp?fileName="+fileName);

    }
	
    poCtrl.webOpen("doc/" + fileName, OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <title></title>
    <link href="images/csstg.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">

        function OnPageOfficeCtrlInit() {
            pageofficectrl.CustomToolbar = false;
        }
        //初始加载模板列表
        function load() {
            if (getQueryString("mb") != null)
                document.getElementById("templateName").value = getQueryString("mb");
        }

        //获取url参数
        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null)
                return unescape(r[2]);
            else
                return null;
        }

        //套红
        function taoHong() {

            var mb = document.getElementById("templateName").value;
            //套红前判断是否修改了正文文件，如果修改了正文文件，提示用户不能直接套红，否则会丢失刚修改的内容
            if (pageofficectrl.IsDirty) {
                var flag = confirm("正文已被修改，如果继续套红会导致刚编辑的内容丢失，是否继续套红？")
                if (flag) {
                    document.getElementById("form1").action = "taoHong.jsp?mb=" + mb;
                    document.forms[0].submit();
                } else {
                    return false;
                }
            }
            document.getElementById("form1").action = "taoHong.jsp?mb=" + mb;
            document.forms[0].submit();
        }

        //保存并关闭
        function saveAndClose() {
            pageofficectrl.WebSave();
            pageofficectrl.CloseWindow();
        }

        function setEnabled(flag) {
            pageofficectrl.Enabled = flag;
        }

        function selectOptionBlur(select) {
            select.blur();
        }
    </script>
</head>
<body onload="load();">
<div id="header">
    <div style="float: left; margin-left: 20px;">
        <img src="images/logo.jpg" height="30"/>
    </div>
    <ul>
        <li>
            <a target="_blank" href="http://www.zhuozhengsoft.com">卓正网站</a>
        </li>
        <li>
            <a target="_blank"
               href="http://www.zhuozhengsoft.com/poask/index.asp">客户问吧</a>
        </li>
        <li>
            <a href="#">在线帮助</a>
        </li>
        <li>
            <a target="_blank"
               href="http://www.zhuozhengsoft.com/about/about/">联系我们</a>
        </li>
    </ul>
</div>
<div id="content">
    <div id="textcontent" style="width: 1000px; height: 800px;">
            <a href="#" onclick="window.external.close();"> <img alt="返回" src="images/return.gif"
                                                                 border="0"/>文件列表</a>
            <span style="width: 100px;"> </span><strong>文档主题：</strong>
            <span style="color: Red;">测试文件</span>
            <form method="post" id="form1">
                <strong>模板列表：</strong>
                <span style="color: Red;"> <select name="templateName"
                                                   id="templateName" style='width: 240px;' onchange="selectOptionBlur(this)" onfocus="setEnabled(false)" onblur="setEnabled(true)">
								<option value='temp2008.doc' selected="selected">
									模板一
								</option>
								<option value='temp2009.doc'>
									模板二
								</option>
								<option value='temp2010.doc'>
									模板三
								</option>
							</select> </span>
                <span style="color: Red;"><input id="taoBut" type="button" value="一键套红"
                                                 onclick="taoHong()"/> </span>
                <span style="color: Red;"><input type="button" value="保存关闭"
                                                 onclick="saveAndClose()"/> </span>
            </form>
        <!--**************   卓正 PageOffice组件 ************************-->
        <%=poCtrl.getHtml()%>
    </div>
</div>
<div id="footer">
    <hr width="1000"/>
    <div>
        Copyright (c) 2012 北京卓正志远软件有限公司
    </div>
</div>
</body>
</html>
