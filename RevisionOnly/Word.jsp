﻿<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    //打开Word文档
    poCtrl.webOpen("doc/test.doc", OpenModeType.docRevisionOnly, "李斯");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>XX文档系统</title>
    <style>
        #main {
            width: 1040px;
            height: 890px;
            border: #83b3d9 2px solid;
            background: #f2f7fb;

        }
        #shut {
            width: 45px;
            height: 30px;
            float: right;
            margin-right: -1px;
        }
        #shut:hover {
        }
    </style>
</head>
<body style="margin:0; padding:0;border:0px; overflow:hidden" scroll="no">
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
        pageofficectrl.AddCustomToolButton("保存", "Save", 1);
        pageofficectrl.AddCustomToolButton("隐藏痕迹", "hideRevision", 18);
        pageofficectrl.AddCustomToolButton("显示痕迹", "showRevision", 9);
    }

    function Save() {
		//设置保存方法
        pageofficectrl.SaveFilePage="/RevisionOnly/SaveFile.jsp"
        pageofficectrl.WebSave();
    }

    function showRevision() {
        pageofficectrl.ShowRevisions = true;
    }

    function hideRevision() {
        pageofficectrl.ShowRevisions = false;
    }
</script>
<div id="main">
    <div id="content" style="height:850px;width:1036px;overflow:hidden;">
        <%=poCtrl.getHtml()%>
    </div>
</div>
</body>
</html>

