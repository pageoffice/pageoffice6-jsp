﻿<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.*" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    WordDocumentWriter wordDoc = new WordDocumentWriter();
    //打开数据区域，openDataRegion方法的参数代表Word文档中的书签名称
    DataRegionWriter dataRegion1 = wordDoc.openDataRegion("PO_image");
    dataRegion1.setEditing(true);//放图片的数据区域是可以编辑的，其它部分不可编辑
    poCtrl.setWriter(wordDoc);

    poCtrl.webOpen("doc/test.doc", OpenModeType.docSubmitForm, "张佚名");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>保存时获取word文档中的图片</title>
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
        pageofficectrl.AddCustomToolButton("保存图片", "Save", 1);
    }
    function Save() {
		//设置保存方法
        pageofficectrl.SaveDataPage="/ExtractImage/SaveData.jsp"
        pageofficectrl.WebSave();
		var value = pageofficectrl.CustomSaveResult;
        alert("保存成功,文件保存到：" +value);
    }
</script>
<div id="div1" style="width: auto; height: 700px;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
