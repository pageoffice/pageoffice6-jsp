<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType, com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl pocCtrl = new PageOfficeCtrl(request);

    String fileName = "template.doc";
    String pdfName = fileName.substring(0, fileName.length() - 4) + ".pdf";
    //打开文件
    pocCtrl.webOpen("doc/" + fileName, OpenModeType.docNormalEdit, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Word文件转换成PDF格式</title>
    <script type="text/javascript">
        function OnPageOfficeCtrlInit() {
            pageofficectrl.AddCustomToolButton("保存", "Save()", 1);
            pageofficectrl.AddCustomToolButton("另存为PDF文件", "SaveAsPDF()", 1);
        }
        //保存
        function Save() {
			//设置保存方法
			pageofficectrl.SaveFilePage="/SaveAsPDF/SaveFile.jsp"
            pageofficectrl.WebSave();
        }

        //另存为PDF文件
        function SaveAsPDF() {
			//设置保存方法
			pageofficectrl.SaveFilePage="/SaveAsPDF/SaveFile.jsp"
            pageofficectrl.WebSaveAsPDF();
            alert("PDF文件已经保存到 /SaveAsPDF/doc目录下。");
            }
    </script>
</head>
<body>
<form id="form1">
    <div id="div1"></div>
    <div style="width: auto; height: 700px;">
        <%=pocCtrl.getHtml()%>
    </div>
</form>
</body>
</html>

