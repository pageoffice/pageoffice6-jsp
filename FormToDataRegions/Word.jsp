﻿<%@ page language="java"
         pageEncoding="utf-8" %>
<%@ page import="static com.zhuozhengsoft.pageoffice.DocumentOpenType.*" %>
<%@ page import="com.zhuozhengsoft.pageoffice.*" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.WordDocumentWriter" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    WordDocumentWriter doc = new WordDocumentWriter();
    doc.setEnableAllDataRegionsEditing(true);
    poCtrl.setWriter(doc);
    poCtrl.webOpen("doc/test.docx", OpenModeType.docSubmitForm, "张佚名");
%>
<!DOCTYPE html>
<html>
<head lang="en">
    <link rel="stylesheet" href="layui/css/layui.css" media="all">
    <script src="layui/layui.js"></script>
    <style>
        html, body{
            margin: 0;
            padding: 0;
            height: 100%;
        }
        .container {
            display: flex;
            width: 100%; /* 容器宽度占满整个窗口 */
            height: 100vh; /* 容器高度占满整个视口高度 */
        }
        .left-panel {
            flex: 1;
        }
        .right-panel {
            flex: 1;
            margin-left: 5px;
            background-color: #f4f4f4;
        }
        h3,h5{
            font-family: "Droid Sans", Arial, sans-serif;
            font-weight: 400;
            margin-bottom: 15px;
        }
        .layui-input,
        .layui-btn
        {
            border-radius: 5px; /* 根据需要调整圆角大小 */
        }
        .layui-form-label{
            white-space: nowrap;
        }
        .layui-text {
            margin-bottom: 20px;
            padding: 10px; /* 内边距 */
        }
        /* 当表格没有数据时的背景色 */
        .layui-none{
            background-color: #FFFFFF; /* 你可以更改为你想要的颜色 */
        }
    </style>

    <script>
        window.onload = function() {
            layui.use(function(){
                var $ = layui.$;
                var form = layui.form;
                var table = layui.table;
                // selectedGood 添加 option
                addGoodsOption($);
                // 监听指定表单的select字段，这里监听的是 name=selectedGood 的select
                form.on('select(selectedGood)', function(){
                    document.getElementById("add-button").classList.remove("layui-btn-disabled");
                    document.getElementById('add-button').disabled = false;
                });
                //table
                var tableData = [];
                //添加货品
                $('#add-button').on('click', function(){
                    addGoods(form,tableData,table)
                });
                //确认填写
                $('#submitPageoffice').on('click', function(){
                    submitPageoffice($);
                });
                layui.form.render();
            });
        }
        function addGoodsOption($){
            var goodsList = [ // 货品列表示例数据
                {id: 1, name: '量子计算机', specification: '550A', model: '入门型', unit: '台', quantity: 1, unit_price: '100RMB'},
                {id: 2, name: '量子计算机', specification: '550C', model: '旗舰型', unit: '台', quantity: 1, unit_price: '200RMB'},
                {id: 3, name: '量子计算机', specification: '550W', model: '发烧型', unit: '台', quantity: 1, unit_price: '300RMB'},
            ];
            $.each(goodsList, function(index, option){
                var text = option.name + option.specification + "," + option.model + "," + option.unit_price + "台";
                var  value = JSON.stringify(goodsList[index]);
                $('#selectedGood').append($('<option>', {
                    value: value,
                    text: text
                }));
            });
        }
        function addGoods(form,tableData,table) {
            var newData = JSON.parse(form.val("form")["selectedGood"]);
            tableData.push(newData);
            table.reload("goodsTable",{
                data: tableData // 这里的'goodsTable'是你的表格的lay-filter值，用于指定要重新加载的表格
            });
            //设置总价
            var total_rmb = parseInt(document.getElementById("total_rmb").innerText) + parseInt((newData.unit_price).replace(/RMB/g, ''));
            document.getElementById("total_rmb").innerText = total_rmb;
        }
        function submitPageoffice($){
            var formData = layui.form.val("form");
            var tableDatas = layui.table.cache['goodsTable'];

            //将form表单中的值回填到word中
            pageofficectrl.word.SetValueToDataRegion('PO_Buyer', formData["purchaser"]);
            pageofficectrl.word.SetValueToDataRegion('PO_Supplier', formData["supplier"]);
            pageofficectrl.word.SetValueToDataRegion('PO_company',formData["buyer_company"] );
            pageofficectrl.word.SetValueToDataRegion('PO_code', formData["project_number"]);
            pageofficectrl.word.SetValueToDataRegion('PO_rmb', document.getElementById("total_rmb").innerText);

            // 获取goods中货品的数量
            let goodsCount = tableDatas.length;
            // 根据goods中货品的数量给表格新增行，模板中默认只有一个表头
            for (let row = 1; row <= goodsCount; row++) {
                pageofficectrl.word.SelectTableCell("PO_table", 1, row, 6);// 选中单元格。在(2,6)这个坐标所在的单元格下方新增行
                pageofficectrl.word.AppendTableRow();// 在当前SelectTableCell方法选中的单元格的下方增加新的一行
            }
            //循环给word  table赋值
            $.each(tableDatas, function(index, row){
                for (let j = 1; j <= 6; j++) {//列
                    pageofficectrl.word.SelectTableCell("PO_table", 1, index + 2, j);// 选中单元格。
                    switch (j) {
                        case 1:
                            pageofficectrl.word.SetTextToSelection(row.name);//给选中的内容赋值
                            break;
                        case 2:
                            pageofficectrl.word.SetTextToSelection(row.specification);
                            break;
                        case 3:
                            pageofficectrl.word.SetTextToSelection(row.model);
                            break;
                        case 4:
                            pageofficectrl.word.SetTextToSelection(row.unit);
                            break;
                        case 5:
                            pageofficectrl.word.SetTextToSelection(row.quantity.toString());
                            break;
                        case 6:
                            pageofficectrl.word.SetTextToSelection(row.unit_price);
                            break;
                    }
                }
            });
        }
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.AddCustomToolButton("保存", "Save", 1);
        }
        function Save() {
            let saveFileUrl = "/FormToDataRegions/SaveFile.jsp";
            pageofficectrl.SaveFilePage = saveFileUrl;
            pageofficectrl.WebSave();
        }
    </script>
</head>
<body>
<div class="container">
    <div class="left-panel">
        <!-- 此div用来加载PageOffice客户端控件，其中div的高宽及位置就决定了控件的大小及位置 -->
        <%=poCtrl.getHtml()%>
    </div>
    <div class="right-panel">
        <h3 style="font-size: 32px;">合同信息</h3>

        <form class="layui-form" id="form " lay-filter="form">
            <!-- 采购人 -->
            <div class="layui-form-item">
                <label class="layui-form-label" >采购人：</label>
                <div class="layui-input-block">
                    <select  name="purchaser">
                        <option value="" >请选择采购人</option>
                        <option value="刘培强">刘培强</option>
                        <option value="图恒宇">图恒宇</option>
                        <option value="周喆直">周喆直</option>
                        <option value="张鹏">张鹏</option>
                        <option value="马兆">马兆</option>
                    </select>
                </div>
            </div>
            <!-- 供应商 -->
            <div class="layui-form-item">
                <label class="layui-form-label">供应商：</label>
                <div class="layui-input-block">
                    <select  name="supplier" >
                        <option value="" >请选择供应商</option>
                        <option value="国盾量子">国盾量子</option>
                        <option value="科大国创">科大国创</option>
                    </select>
                </div>
            </div>
            <!-- 采购方企业： -->
            <div class="layui-form-item">
                <label class="layui-form-label">采购方企业：</label>
                <div class="layui-input-block">
                    <select  name="buyer_company">
                        <option value="">请选择采购方企业</option>
                        <option value="卓正软件">卓正软件</option>
                        <option value="卓正PageOffice">卓正PageOffice</option>
                    </select>
                </div>
            </div>
            <!-- 项目编号 -->
            <div class="layui-form-item">
                <label class="layui-form-label">项目编号：</label>
                <div class="layui-input-block">
                    <input type="text" name="project_number"   placeholder="请输入项目编号" autocomplete="off" class="layui-input">
                </div>
            </div>

            <hr style="height:1px;background-color: #d2d2d2;"><br/>

            <div class="layui-form-item">
                <label class="layui-form-label" >
                    <button type="button" style="" class="layui-btn layui-btn-sm layui-btn-normal layui-btn-disabled" disabled id="add-button" >添加到清单</button>
                </label>
                <div class="layui-input-block ">
                    <select  name="selectedGood" id="selectedGood" lay-filter="selectedGood">
                        <option value="" >请选择货品</option>
                    </select>
                </div>
            </div>

            <h5 style="font-size: 18px;">采购货品清单:</h5>
            <!-- 采购货品清单表格 -->
            <table id="goodsTable" lay-data="{id:'goodsTable'}" lay-filter="goodsTable" class="layui-table" lay-skin="line" style="white-space: nowrap;">
                <thead>
                <tr>
                    <th lay-data="{field:'name'}">货物品名</th>
                    <th lay-data="{field:'specification'}">规格</th>
                    <th lay-data="{field:'model'}">型号</th>
                    <th lay-data="{field:'unit'}">单位</th>
                    <th lay-data="{field:'quantity'}">数量</th>
                    <th lay-data="{field:'unit_price'}">单价</th>
                    <%--<th lay-data="{field:'action',align:'center', toolbar: '#deleteBtn'}">操作</th>--%>
                </tr>
                </thead>
            </table>
            <script type="text/html" id="deleteBtn">
                <a class="layui-btn  layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>
            </script>
            <div class="layui-text" style="text-align: left;">
                <p>
                    合同总价：
                    <span id="total_rmb" >0</span>
                    RMB
                </p>
            </div>
            <div class="layui-input-block" style="text-align: center;">
                <button type="reset" class="layui-btn layui-btn-primary ">重置</button>
                <button type="button" class="layui-btn layui-btn-normal" id="submitPageoffice" >确认填写</button>
            </div>

        </form>
    </div>

</div>
</body>
</html>
