﻿<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    //******************************卓正PageOffice组件的使用*******************************
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    //打开文件
    poCtrl.webOpen("doc/test.doc", OpenModeType.docRevisionOnly, "Tom");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        pageofficectrl.AddCustomToolButton("保存", "Save", 1);
    }
    function Save() {
		//设置保存方法
        pageofficectrl.SaveFilePage="/RevisionsList/SaveFile.jsp"
        pageofficectrl.WebSave();
    }

    function AfterDocumentOpened() {
        refreshList();
    }

    //获取当前痕迹列表
    function refreshList() {
        //alert(pageofficectrl.word.RevisionsAsJson);
        let revisionListJson = JSON.parse(pageofficectrl.word.RevisionsAsJson);
        var i;
        document.getElementById("ul_Comments").innerHTML = "";
        for (let item of revisionListJson) {
            var str = "";
            str = str + item.author;

            var revisionDate = item.date ;
            //转换为标准时间
            str = str + " " + dateFormat(revisionDate, "yyyy-MM-dd HH:mm:ss");

            if (item.type == "1") {
                str = str + ' 插入：' + pageofficectrl.word.GetTextFromRevision(parseInt(item.id));
            }
            else if (item.type == "2") {
                str = str + ' 删除：' + pageofficectrl.word.GetTextFromRevision(parseInt(item.id));
            }
            else {
                str = str + ' 调整格式或样式。';
            }
            document.getElementById("ul_Comments").innerHTML += "<li><a href='#' onclick='goToRevision(" + item.id + ")'>" + str + "</a></li>"
        }

    }

    //GMT时间格式转换为CST
    function dateFormat(date, format) {
        var date= new Date((date - 25569) * 86400 * 1000);
        date.setHours(date.getHours() - 8);
        var o = {
            'M+': date.getMonth() + 1, //month
            'd+': date.getDate(), //day
            'H+': date.getHours(), //hour
            'm+': date.getMinutes(), //minute
            's+': date.getSeconds(), //second
            'q+': Math.floor((date.getMonth() + 3) / 3), //quarter
            'S': date.getMilliseconds() //millisecond
        };

        if (/(y+)/.test(format))
            format = format.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));

        for (var k in o)
            if (new RegExp('(' + k + ')').test(format))
                format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length));

        return format;
    }

    //定位到当前痕迹
    function goToRevision(index) {
        pageofficectrl.word.SelectRevision(index);
    }
    //刷新列表
    function refresh_click() {
        refreshList();
    }

</script>
<div style=" width:1300px; height:700px;">
    <div style=" width:1300px; height:700px;">
        <div id="Div_Comments" style=" float:left; width:200px; height:700px; border:solid 1px red;">
            <h3>痕迹列表</h3>
            <input type="button" name="refresh" value="刷新" onclick="refresh_click()"/>
            <ul id="ul_Comments">

            </ul>
        </div>
        <div style=" width:1050px; height:700px; float:right;">
            <div style="height: 800px; width: auto"  ><%=poCtrl.getHtml()%></div>
        </div>
    </div>

</div>
</body>
</html>

