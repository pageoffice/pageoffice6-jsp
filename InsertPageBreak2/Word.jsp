<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl, com.zhuozhengsoft.pageoffice.word.*"
         pageEncoding="utf-8" %>
<%
    WordDocumentWriter doc = new WordDocumentWriter();
    DataRegionWriter mydr1 = doc.createDataRegion("PO_first", DataRegionInsertType.After, "[end]");
    mydr1.selectEnd();
    doc.insertPageBreak();//插入分页符
    DataRegionWriter mydr2 = doc.createDataRegion("PO_second", DataRegionInsertType.After, "[end]");
    mydr2.setValue("[word]doc/test2.doc[/word]");

    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    poCtrl.setWriter(doc);

    //打开Word文档
    poCtrl.webOpen("doc/test1.doc", OpenModeType.docNormalEdit, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>在word文档中光标处插入分页符</title>
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
        pageofficectrl.AddCustomToolButton("保存", "Save", 1);
    }
    function Save() {
		//设置保存方法
        pageofficectrl.SaveFilePage="/InsertPageBreak2/SaveFile.jsp"
        pageofficectrl.WebSave();
		if (pageofficectrl.CustomSaveResult == "ok") {
            alert("保存成功！请在/InsertPageBreak2/doc目录下查看合并后的新文档\"test3.doc\"。");
        }
    }
</script>
<div style=" width:auto; height:700px;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
