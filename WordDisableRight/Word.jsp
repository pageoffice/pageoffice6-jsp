<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,com.zhuozhengsoft.pageoffice.word.WordDocumentWriter,javax.servlet.*,javax.servlet.http.*"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
     //此行必须
    WordDocumentWriter wordDoc = new WordDocumentWriter();
    wordDoc.setDisableWindowRightClick(true);//禁止word鼠标右键
    poCtrl.setWriter(wordDoc);
    //打开文件
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>禁止Word文档中鼠标右键</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数

        pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
    }

</script>
	<div style="color:Red">打开Word文档后，鼠标右键，发现右键失效。</div>
    <div style=" width:auto; height:700px;">
        <%=poCtrl.getHtml()%>
    </div>
</body>
</html>
