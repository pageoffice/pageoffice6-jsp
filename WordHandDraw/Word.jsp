<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    poCtrl.webOpen("doc/template.doc", OpenModeType.docNormalEdit, "张三");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title></title>
    <script language="JavaScript">
        function Save() {
			//设置保存文件方法
            pageofficectrl.SaveFilePage="/WordHandDraw/SaveFile.jsp"
			pageofficectrl.WebSave();
		}
		function StartHandDraw(){
			pageofficectrl.word.StartHandDraw();
		}
		function OnPageOfficeCtrlInit() {
			pageofficectrl.AddCustomToolButton("保存", "Save()", 1);
			pageofficectrl.AddCustomToolButton("开始手写", "StartHandDraw()", 5);
		}
    </script>
</head>
<body>
    <div style="height: 98vh; width: auto;">
        <%=poCtrl.getHtml()%>
    </div>
</body>
</html>
