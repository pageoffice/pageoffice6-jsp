<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    //打开Word文档
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张佚名");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
        pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
    }

    function increaseCount(value) {
        var pJson={};
        pJson.value=value;
        var pstr=JSON.stringify(pJson);
        CallParentFunc({
            funcName: "updateCount",
            paramJson: pstr,
			success: function (strRet) {
				alert("现在父窗口Count的值为：" + strRet);
			},
			error: function (strRet) {
				if (strRet.indexOf('parentlost') > -1) {
					alert('error: 父页面关闭或跳转刷新了，导致父页面函数没有调用成功！');
				}else{
					console.error(strRet);
				} 
			}
    });
    }

    function increaseCountAndClose(value) {
        var pJson={};
        pJson.value=value;
        var pstr=JSON.stringify(pJson);
        CallParentFunc({
            funcName: "updateCount",
            paramJson: pstr,
			success: function (strRet) {
				alert("现在父窗口Count的值为：" + strRet);
                pageofficectrl.CloseWindow();
			},
			error: function (strRet) {
				if (strRet.indexOf('parentlost') > -1) {
					alert('error: 父页面关闭或跳转刷新了，导致父页面函数没有调用成功！');
				}else{
					console.error(strRet);
				} 
			}
			});

    }
</script>
<input type="button" value="设置父窗口Count的值加1" onclick="increaseCount(1);"/>
<input type="button" value="设置父窗口Count的值加5，并关闭窗口" onclick="increaseCountAndClose(5);"/></br>
<div style=" width:auto; height:700px;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>

