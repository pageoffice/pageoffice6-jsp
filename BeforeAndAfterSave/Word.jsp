<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>文档保存前和保存后执行的事件</title>
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
        pageofficectrl.AddCustomToolButton("保存", "Save", 1);
    }

    function Save() {
		//在这里写您保存前的代码
		alert("文件就要开始保存了");
		//设置保存方法
        pageofficectrl.SaveFilePage="/BeforeAndAfterSave/SaveFile.jsp"
        pageofficectrl.WebSave();
        //在这里写您保存后的代码，比如判断保存结果pageofficectrl.CustomSaveResult
        if ("ok" == pageofficectrl.CustomSaveResult) {
            alert("文件已经保存成功了");
        } else {
            alert("保存失败！");
        }
    }

</script>
<form id="form1">
    <div style=" width:auto; height:700px;">
        <%=poCtrl.getHtml()%>
    </div>
</form>
</body>
</html>
