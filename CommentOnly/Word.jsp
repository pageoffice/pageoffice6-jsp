﻿<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    
    poCtrl.webOpen("doc/test.doc", OpenModeType.docCommentOnly, "李斯");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>CommentOnly</title>
	<script type="text/javascript">
		function Save() {
			//设置保存方法
            pageofficectrl.SaveFilePage="/CommentOnly/SaveFile.jsp"
			pageofficectrl.WebSave();
		}

		function newComment() {
			pageofficectrl.word.InsertComment();
		}
		function OnPageOfficeCtrlInit() {
			pageofficectrl.AddCustomToolButton("保存", "Save()", 1);
			pageofficectrl.AddCustomToolButton("插入批注", "newComment()", 0);
		}		
	</script>
</head>
<body>
<div style="height: 98vh; width: auto;">
	<%=poCtrl.getHtml()%>
</div>
</body>
</html>

