<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>

<%@ page import="com.zhuozhengsoft.pageoffice.word.WordDocumentWriter" %>
<%
	PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

	WordDocumentWriter doc = new WordDocumentWriter();
	doc.getTemplate().defineDataTag("{ 甲方 }");
	doc.getTemplate().defineDataTag("{ 乙方 }");
	doc.getTemplate().defineDataTag("{ 担保人 }");
	doc.getTemplate().defineDataTag("【 合同日期 】");
	doc.getTemplate().defineDataTag("【 合同编号 】");

	poCtrl.setWriter(doc);
	//打开Word文档
	poCtrl.webOpen("/DataTagEdit/doc/test.docx", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title></title>
    <style>
        body {
            margin: 0;
            padding: 0;
            display: flex;
        }
        #left-container {
            width: 360px;
            display: flex;
            flex-direction: column;
            border-right: 2px solid #ccc;
            padding: 20px;
            overflow: auto;
            font-size: 12px;
            height: 90vh;
        }
        #right-container {
            flex: 1;
            padding: 0px;
            height: 95vh;
        }
        #podiv{
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #left-title{
            text-align: center;
            font-size: 16px;
            padding-bottom: 10px;
            margin-bottom: 10px;
            border-bottom: solid 1px #ccc;
        }

        .input-group {
            margin-bottom: 20px;
            display: flex;
            align-items: center;
        }
        input[type="text"] {
            width: 70%;
            padding: 10px;
            margin-top: 5px;
            box-sizing: border-box;
            border: 1px solid #ccc;
            border-radius: 5px;
            font-size: 12px;
            outline: none;
        }

        input[type="submit"] {
            width: 80px;
            padding: 10px;
            margin-top: 5px;
            margin-left: 10px;
            box-sizing: border-box;
            border: none;
            border-radius: 5px;
            background-color: #4E6EF2;
            color: white;
            font-size: 12px;
            outline: none;
            cursor: pointer;
        }
        /* 表格样式 */
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        th {
            position: sticky;
            top: 0;
            background-color: #f2f2f2;
            z-index: 1;
        }

        /* 容器样式 */
        .container {
            height: 600px;
            overflow: auto;
            border: solid 1px #ccc;
            scrollbar-width: thin;
            scrollbar-color: #888 #f2f2f2;
        }

        /* 滚动条样式 */
        .container::-webkit-scrollbar {
            width: 8px;
        }

        .container::-webkit-scrollbar-track {
            background: #f2f2f2;
        }

        .container::-webkit-scrollbar-thumb {
            background-color: #888;
            border-radius: 4px;
        }

        .container::-webkit-scrollbar-thumb:hover {
            background-color: #555;
        }
        .delete-button {
            padding: 6px 6px;
            border: none;
            border-radius: 5px;
            background-color: #f44336;
            color: white;
            font-size: 12px;
            cursor: pointer;
        }
        .delete-button:hover {
            background-color: #d32f2f;
        }

        .normal-button {
            padding: 6px 6px;
            border: none;
            border-radius: 5px;
            background-color: #4E7EFF;
            color: white;
            font-size: 12px;
            cursor: pointer;
        }
        .normal-button:hover {
            background-color: #4E6EF2;
        }

        .locate-button {
            padding: 6px 6px;
            border: none;
            border-radius: 5px;
            background-color: #0abb87;
            color: white;
            font-size: 12px;
            cursor: pointer;
        }
        .locate-button:hover {
            background-color: #0a9966;
        }
    </style>
    <script>
        var definedDataTagJson = '';
        var isFromStart = false;
        var lastOpTag = '';

        function OnPageOfficeCtrlInit() {
            pageofficectrl.AddCustomToolButton("保存", "Save", 1);
        }
        function Save() {
			//设置保存方法
            pageofficectrl.SaveFilePage="/DataTagEdit/SaveFile.jsp";
            pageofficectrl.WebSave();
        }

        //加载数据
        function loadData() {
            var kWord = document.getElementById("inputKey").value;
            searchDataTag(definedDataTagJson, kWord);
            return;
        }

        //加载上方数据列表
        function searchDataTag(dtDefinedJson, s){
            var tb1 = document.getElementById("tagTable");
            var rCount = tb1.rows.length;
            for (var i = 1; i < rCount; i++) {
                tb1.deleteRow(1);
            }

            if('' == dtDefinedJson) dtDefinedJson = '[]';
            let definedDataTagObj = JSON.parse(dtDefinedJson);

            var oTable = document.getElementById("tagTable");
            var tbodyObj = oTable.tBodies[0];
            for(let key in definedDataTagObj ){
                let dtName = definedDataTagObj[key].name;
                if (dtName.toLocaleLowerCase().indexOf(s.toLocaleLowerCase()) > -1) {
                    var oTr = tbodyObj.insertRow();
                    var oTd = oTr.insertCell();
                    oTd.innerHTML = dtName;
                    oTd.title = dtName;
                    oTd = oTr.insertCell();
                    oTd.innerHTML = '<button class="delete-button" onclick="deleteTag(\''+ dtName +'\')">删除</button> <button class="locate-button" onclick="locateTag(\''+ dtName +'\')" >定位</button> <button class="normal-button" onclick="addTag(\''+ dtName +'\');">添加</button> ';
                }
            }
        }

        function addTag(tagName) {
            pageofficectrl.word.SetTextToSelection(tagName);
        }

        function locateTag(tagName){
            pageofficectrl.word.SelectionCollapse(0);

            if(isFromStart){
                if(lastOpTag == tagName){
                    pageofficectrl.word.HomeKey(6);
                }

                isFromStart = false;
            }

            if(!pageofficectrl.word.FindNextText(tagName)){
                alert('已经搜索到文档末尾。');
                isFromStart = true;
            }

            lastOpTag = tagName;
        }

        function deleteTag(tagName){
            let selectText = pageofficectrl.word.GetTextFromSelection();
            if(tagName != selectText){
                alert('请先执行‘'+tagName+'’的定位操作，然后再删除。');
            }else{
                pageofficectrl.word.SetTextToSelection('');
            }
        }

        function AfterDocumentOpened() {
            definedDataTagJson = pageofficectrl.word.DataTagsDefinedAsJson;
            loadData();
        }

    </script>

</head>
<body>
    <div id="left-container">
        <div id="left-title">定义数据标签</div>
        <div class="input-group">
            <span style="font-size: 14px;">待添加标签：</span><input type="text" id="inputKey" oninput="loadData();" placeholder="请输入数据标签关键字搜索">
        </div>
        <div class="container">
            <table id="tagTable">
                <thead>
                <tr>
                    <th>数据标签</th>
                    <th style="width: 130px;">操作</th>
                </tr>
                </thead>
                <tbody>
                <!-- 大量的数据行 -->
                <!--<tr>-->
                <!--<td>AAA</td>-->
                <!--<td>BBB</td>-->
                <!--<td><button class="normal-button">添加</button></td>-->
                <!--</tr>-->

                </tbody>
            </table>
        </div>

    </div>
    <div id="right-container" >
        <div id="podiv" >
            <%=poCtrl.getHtml()%>
        </div>
        <!-- 右侧内容 -->
    </div>

</body>
</html>
