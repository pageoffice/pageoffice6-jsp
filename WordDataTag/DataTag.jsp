<%@ page language="java" import="com.zhuozhengsoft.pageoffice.OpenModeType"
import="com.zhuozhengsoft.pageoffice.PageOfficeCtrl ,com.zhuozhengsoft.pageoffice.word.* ,java.awt.*,java.text.SimpleDateFormat,java.util.Date"
         pageEncoding="utf-8" %>
<%
    //PageOffice组件的使用

    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    //定义WordDocument对象
    WordDocumentWriter doc = new WordDocumentWriter();
    //定义DataTag对象
    DataTagWriter deptTag = doc.openDataTag("{部门名}");
    //给DataTag对象赋值
    deptTag.setValue("B部门");
    deptTag.getFont().setColor(Color.GREEN);

    DataTagWriter userTag = doc.openDataTag("{姓名}");
    userTag.setValue("李四");
    userTag.getFont().setColor(Color.GREEN);

    DataTagWriter dateTag = doc.openDataTag("【时间】");
    dateTag.setValue(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString());
    dateTag.getFont().setColor(Color.BLUE);

    poCtrl.setWriter(doc);
    //打开Word文件
    poCtrl.webOpen("doc/test2.doc", OpenModeType.docNormalEdit, "张佚名");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
</head>
<body>
    <script type="text/javascript">
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.CustomToolbar = false;
        }
    </script>
	<div style=" width:auto; height:700px;">
		<%=poCtrl.getHtml()%>
	</div>
</body>
</html>
