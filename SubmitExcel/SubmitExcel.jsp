<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.excel.*" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    //定义Workbook对象
    WorkbookWriter workBook = new WorkbookWriter();
    //定义Sheet对象，"Sheet1"是打开的Excel表单的名称
    SheetWriter sheet = workBook.openSheet("Sheet1");
    //定义table对象，设置table对象的设置范围
    ExcelTableWriter table = sheet.openTable("B4:F13");
    //设置table对象的提交名称，以便保存页面获取提交的数据
    table.setSubmitName("Info");
    poCtrl.setWriter(workBook);

    //打开excel文档
    poCtrl.webOpen("doc/test.xls", OpenModeType.xlsSubmitForm, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>最简单的提交Excel中的数据</title>
    <script type="text/javascript">
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.AddCustomToolButton("保存", "Save", 1);
        }
        function Save() {
			 //设置保存方法
            pageofficectrl.SaveDataPage="/SubmitExcel/SaveData.jsp"
            //在这里写您保存前的代码
            pageofficectrl.WebSave();
            //在这里写您保存后的代码，比如判断保存结果pageofficectrl.CustomSaveResult
            alert(pageofficectrl.CustomSaveResult);
        }
    </script>
</head>
<body>
<form id="form1">
    <div style="width: auto; height: 700px;">
        <%=poCtrl.getHtml()%>
    </div>
</form>
</body>
</html>
