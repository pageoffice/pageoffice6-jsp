<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.excel.*, java.text.DecimalFormat"
         pageEncoding="utf-8" %>
<%@ page import="java.text.NumberFormat" %>
<%
    WorkbookReader workBook = new WorkbookReader(request, response);
    SheetReader sheet = workBook.openSheet("Sheet1");
    ExcelTableReader table = sheet.openTable("Info");
    String content = "";
    int result = 0;
    while (!table.getEOF()) {
        //获取提交的数值
        if (!table.getDataFields().getIsEmpty()) {
            content += "\r\n月份名称："
                    + table.getDataFields().get(0).getText();
            content += "\r\n计划完成量："
                    + table.getDataFields().get(1).getText();
            content += "\r\n实际完成量："
                    + table.getDataFields().get(2).getText();
            content += "\r\n累计完成量："
                    + table.getDataFields().get(3).getText();
            //out.print(table.getDataFields().get(2).getText()+"      mmmmmmmmmmmmm          "+table.getDataFields().get(1).getText());
            if (table.getDataFields().get(2).getText().equals(null)
                    || table.getDataFields().get(2).getText().trim().length() == 0
            ) {
                content += "\r\n完成率：0%";
            } else {
                float f = Float.parseFloat(table.getDataFields().get(2)
                        .getText());
                f = f / Float.parseFloat(table.getDataFields().get(1).getText());
                DecimalFormat df = (DecimalFormat) NumberFormat.getInstance();
                content += "\r\n完成率：" + df.format(f * 100) + "%";
            }
            content += "\r\n*********************************************";
        }
        //循环进入下一行
        table.nextRow();
    }
    table.close();
    /**
     * 实际开发中，一般获取数据区域的值后用来和数据库进行交互，比如根据刚才获取的数据进行数据库记录的新增，更新，删除等。
     * 此处为了给用户展示获取的数据内容，通过setCustomSaveResult将获取的数据区域的值返回到前端页面给用户检查执行的结果。
     * 如果只是想返回一个保存结果，可以使用比如：setCustomSaveResult("ok")，前端可以根据这个保存结果进行下一步逻辑处理。
     */
    workBook.setCustomSaveResult(content);
    workBook.close();
%>
