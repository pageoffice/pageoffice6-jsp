<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.excel.*" %>
<%

    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    //定义Workbook对象
    WorkbookWriter workBook = new WorkbookWriter();
    //定义Sheet对象，"Sheet1"是打开的Excel表单的名称
    SheetWriter sheet = workBook.openSheet("Sheet1");
    //定义Table对象
    ExcelTableWriter table = sheet.openTable("B4:F11");
    int rowCount = 12;//假设将要自动填充数据的实际记录条数为12
    for (int i = 1; i <= rowCount; i++) {
        //给区域中的单元格赋值
        table.getDataFields().get(0).setValue(i + "月");
        table.getDataFields().get(1).setValue("100");
        table.getDataFields().get(2).setValue("120");
        table.getDataFields().get(3).setValue("500");
        table.getDataFields().get(4).setValue("120%");
        table.nextRow();//循环下一行，此行必须
    }
    //关闭table对象
    table.close();
    //定义Table对象
    ExcelTableWriter table2 = sheet.openTable("B13:F16");
    int rowCount2 = 4;//假设将要自动填充数据的实际记录条数为12
    for (int i = 1; i <= rowCount2; i++) {
        //给区域中的单元格赋值
        table2.getDataFields().get(0).setValue(i + "季度");
        table2.getDataFields().get(1).setValue("300");
        table2.getDataFields().get(2).setValue("300");
        table2.getDataFields().get(3).setValue("300");
        table2.getDataFields().get(4).setValue("100%");
        table2.nextRow();
    }
    //关闭table对象
    table2.close();
    poCtrl.setWriter(workBook);


    //打开excel文件
    poCtrl.webOpen("doc/test4.xls", OpenModeType.xlsNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>给Excel文档中定义名称的区域赋值</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">

</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数
        pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
    }

</script>

openTable填充数据后显示的效果
<div style="width: 1000px; height: 900px;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
