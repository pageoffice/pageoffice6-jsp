<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.excel.*" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.GregorianCalendar" %>
<%

    String tempFileName = request.getParameter("temp");

    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    //定义Workbook对象
    WorkbookWriter workBook = new WorkbookWriter();
    //定义Sheet对象，"Sheet1"是打开的Excel表单的名称
    SheetWriter sheet = workBook.openSheet("Sheet1");
    //定义Table对象，参数“report”就是Excel模板中定义的单元格区域的名称
    ExcelTableWriter table = sheet.openTableByDefinedName("report", 10, 5, true);
    //给区域中的单元格赋值
    table.getDataFields().get(0).setValue("轮胎");
    table.getDataFields().get(1).setValue("100");
    table.getDataFields().get(2).setValue("120");
    table.getDataFields().get(3).setValue("500");
    table.getDataFields().get(4).setValue("120%");
    //循环下一行
    table.nextRow();
    //关闭table对象
    table.close();
    //定义单元格对象，参数“year”就是Excel模板中定义的单元格的名称
    ExcelCellWriter cellYear = sheet.openCellByDefinedName("year");
    // 给单元格赋值
    Calendar c = new GregorianCalendar();
    int year = c.get(Calendar.YEAR);//获取年份
    cellYear.setValue(year + "年");
    ExcelCellWriter cellName = sheet.openCellByDefinedName("name");
    cellName.setValue("张三");

    poCtrl.setWriter(workBook);

    //打开excel文件
    poCtrl.webOpen("doc/" + tempFileName, OpenModeType.xlsNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>给Excel文档中定义名称的区域赋值</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数
        pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
    }

</script>

模板一填充数据后显示的效果
<div style="width: 1000px; height: 900px;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
