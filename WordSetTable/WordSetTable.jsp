<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.*" %>
<%
    //******************************卓正PageOffice组件的使用*******************************
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    WordDocumentWriter doc = new WordDocumentWriter();
    //打开数据区域
    DataRegionWriter dataRegion = doc.openDataRegion("PO_regTable");
    //打开table，openTable(index)方法中的index代表Word文档中table位置的索引，从1开始
    WordTableWriter table = dataRegion.openTable(1);
    //给table中的单元格赋值， openCellRC(int,int)中的参数分别代表第几行、第几列，从1开始
    table.openCellRC(3, 1).setValue("A公司");
    table.openCellRC(3, 2).setValue("开发部");
    table.openCellRC(3, 3).setValue("李清");
    //插入一行，insertRowAfter方法中的参数代表在哪个单元格下面插入一个空行
    table.insertRowAfter(table.openCellRC(3, 3));
    table.openCellRC(4, 1).setValue("B公司");
    table.openCellRC(4, 2).setValue("销售部");
    table.openCellRC(4, 3).setValue("张三");

    poCtrl.setWriter(doc);

    //打开文件
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>简单的给Word文档中的Table赋值</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
</head>
<body>
    <script type="text/javascript">
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.CustomToolbar = false;
        }
    </script>
	<div style=" width:auto; height:700px;">
		<%=poCtrl.getHtml()%>
	</div>
</body>
</html>
