<%@ page language="java" import="com.zhuozhengsoft.pageoffice.OpenModeType" pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.PageOfficeCtrl" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.*" %>

<%
    String userName = request.getParameter("userName");
    //***************************卓正PageOffice组件的使用********************************
    WordDocumentWriter doc = new WordDocumentWriter();
    //打开数据区域
    DataRegionWriter d1 = doc.openDataRegion("PO_com1");
    DataRegionWriter d2 = doc.openDataRegion("PO_com2");

    //给数据区域赋值
    d1.setValue("[word]doc/content1.doc[/word]");
    d2.setValue("[word]doc/content2.doc[/word]");

    //根据登录用户名设置数据区域可编辑性
    //甲客户：zhangsan登录后
    if (userName.equals("zhangsan")) {
        d1.setEditing(true);
        d2.setEditing(false);
       //若要将数据区域内容存入文件中，则必须设置属性“setSubmitAsFile”值为true
       d1.setSubmitAsFile(true);
    }
    //乙客户：lisi登录后
    else {
        d2.setEditing(true);
        d1.setEditing(false);
       //若要将数据区域内容存入文件中，则必须设置属性“setSubmitAsFile”值为true
       d2.setSubmitAsFile(true);
    }

    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    poCtrl.setWriter(doc);

    //设置文档打开方式
    poCtrl.webOpen("doc/test.doc", OpenModeType.docSubmitForm, userName);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <title></title>
    <link href="images/csstg.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="content">
    <div id="textcontent" style="width: 1000px; height: 800px;">
        <div class="flow4">
            <input type="button" onclick="exit()" value="关闭窗口" />
            <strong>当前用户：</strong>
            <span style="color: Red;"><%=userName%></span>
        </div>

        <script type="text/javascript">
            function OnPageOfficeCtrlInit() {
                // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
                pageofficectrl.AddCustomToolButton("保存", "Save", 1);
                pageofficectrl.AddCustomToolButton("全屏/还原", "IsFullScreen", 4);
            }

            //保存
            function Save() {
				//设置保存方法
				pageofficectrl.SaveDataPage="/SetDrByUserWord2/SaveData.jsp?userName=" +"<%= userName%>";
                pageofficectrl.WebSave();
            }

            //全屏/还原
            function IsFullScreen() {
                pageofficectrl.FullScreen = !pageofficectrl.FullScreen;
            }
            function exit(){
                pageofficectrl.CloseWindow();
            }
        </script>
        <!--**************   卓正 PageOffice组件 ************************-->
        <%=poCtrl.getHtml()%>
    </div>
</div>
</body>
</html>

