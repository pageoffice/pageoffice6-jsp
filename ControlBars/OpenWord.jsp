<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    //******************************卓正PageOffice组件的使用*******************************
    //设置PageOffice服务器组件
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    //打开文件
    poCtrl.webOpen("doc/template.doc", OpenModeType.docReadOnly, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>控制标题栏、自定工具栏和Office工具栏的隐藏和显示</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <script>
        function OnPageOfficeCtrlInit() {
            pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
            pageofficectrl.OfficeToolbars = false; //隐藏Office工具栏
            pageofficectrl.Titlebar = false; //隐藏标题栏
        }
    </script>
</head>
<body>
隐藏了标题栏、自定工具栏和Office工具栏的效果，每个栏都是可以单独的控制是否隐藏。
<div style=" width:auto; height:700px;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
