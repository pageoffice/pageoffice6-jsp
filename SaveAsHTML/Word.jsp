<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    //打开Word文档
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Word文件另存为HTML</title>
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
        pageofficectrl.AddCustomToolButton("另存HTML", "saveAsHTML", 1);

    }
    function saveAsHTML() {
		//设置保存方法
        pageofficectrl.SaveFilePage="/SaveAsHTML/SaveFile.jsp"
        pageofficectrl.WebSaveAsHTML();
        alert("HTML格式的文件已经保存到 /SaveAsHTML/doc 目录下。");
    }
</script>
<form id="form1">
    <div id="div1">
        <a href='doc/test.htm'> 查看另存的html文件</a><br>
    </div>
    <div style=" width:1000px; height:800px;">
        <%=poCtrl.getHtml()%>
    </div>
</form>
</body>
</html>
