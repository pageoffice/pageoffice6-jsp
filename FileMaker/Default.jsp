<%@ page language="java" pageEncoding="utf-8" %>
<%
  
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title></title>
	<script type="text/javascript" src="../pageoffice.js"></script>
    <script type="text/javascript">
		var ids = []; 
		
		function ConvertFiles() {
			var checkboxes = document.getElementsByName('company');
		
			for (var i = 0; i < checkboxes.length; i++) {  
				if (checkboxes[i].checked) { // 如果checkbox被选中  
					ids.push(checkboxes[i].value);  
				}  
			}  
			
			if(0 == ids.length){
				alert('请至少选择一个公司');
				return;
			}
			
			document.getElementById("Button1").disabled = true;
			ConvertFile(ids, 0);
		}

        function ConvertFile(idArr, index) {
			//设置保存方法
            filemakerctrl.SaveFilePage="/FileMaker/SaveMaker.jsp?id="+idArr[index];
            //注意：CallFileMaker是pageoffice.js中封装好的方法，专门供调用FileMakerCtrl生成文件使用
            filemakerctrl.CallFileMaker({
                //url：指向服务器端FileMakerCtrl生成文件的controller地址
                url: "/FileMaker/FileMaker.jsp?id="+idArr[index],
                success: function (res) {//res：获取服务器端fs.setCustomSaveResult设置的保存结果
					console.log(res);
                    console.log("completed successfully.");
                    setProgress1(100);

                    index++;
                    setProgress2(index, idArr.length);

                    if(index < idArr.length){
                        ConvertFile(idArr, index);
                    }
                },
                progress: function (pos) {
                    console.log("running "+pos+"%");
                    setProgress1(pos);
                },
                error: function (msg) {
                    document.getElementById("errorMsg").innerHTML = "发生错误: <br /> " + msg;
                    console.log("error occurred: "+msg);
                }
            });

        }
		
		function setProgress1(percent) {
			var progressBar = document.getElementById("progressBar1");
			progressBar.style.width = percent + '%';
			progressBar.innerText = percent + '%';
		}
		
		function setProgress2(index, count) {
			var progressBar = document.getElementById("progressBar2");
			progressBar.style.width = Math.round(index/count*100) + '%';
			progressBar.innerText = index + '/' + count;
		}
    </script>
	<style>
		.progressBarContainer {
		  width: 100%;
		  background-color: #eee;
		  border-radius: 5px;
		  padding: 3px;
		  box-shadow: 2px 2px 3px 3px #ccc inset;  
		}

		.progressBar {
		  height: 20px;
		  width: 0%;
		  background-color: #1A73E8;
		  border-radius: 5px;
		  text-align: center;
		  line-height: 20px; /* 使文字垂直居中 */
		  color: white;
		}
		
		#progressDiv{
			width:400px;
			margin: 10px auto;
			text-align: left;
			font-size:14px;
			border: solid 1px #1A73E8;
			padding:10px 20px;
			color: #1A73E8;
		}
		#errorMsg{
			color: red;
		}
	</style>
	<style>  
		/* 样式定义 */  
		.company-list {  
		  list-style-type: none;  
		  padding: 0;  
		  margin: 0 auto;  
		  width: 400px;
		}  
		  
		.company-list li {  
		  display: flex;  
		  justify-content: space-between;  
		  align-items: center;  
		  margin-bottom: 10px;  
		}  
		  
		.company-list label {  
		  display: block;  
		  font-weight: bold;  
		  margin-bottom: 5px;  
		}  
		  
		.company-list input[type="checkbox"] {  
		  margin-right: 5px;  
		}  
  </style>
</head>
<body>

    <div style="text-align: center;">
		<h3>演示：填充数据到模板中批量生成word文件</h3>
		<div style="width:600px;margin: 0 auto; font-size:14px;">
			<p style="text-align: left;">
				演示内容：<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;本示例演示了批量生成荣誉证书的效果。选择需要生成荣誉证书的公司，然后点击“批量生成Word文件”按钮，就可以把各个公司名动态填充到荣誉证书模板“template.doc”中，为每个公司生成一份荣誉证书文件。
			</p>
			<p style="text-align: left;">
				操作说明：<br/>
				1. 勾选下面的公司名称；<br/>
				2. 点击“批量生成Word文件”按钮；<br/>
				3. 生成完毕后，即可在“FileMaker/doc”目录下看到批量生成的Word文件。<br/>
			</p>
		</div>
		
		<hr />
		<ul class="company-list">  
			<li>  
			  <label> 1 
				<input name="company" value="1" type="checkbox" /> 微软（中国）有限公司  
			  </label>  
			</li>  
			<li>  
			  <label> 2 
				<input name="company" value="2" type="checkbox" /> IBM（中国）服务有限公司  
			  </label>  
			</li>  
			<li>  
			  <label> 3 
				<input name="company" value="3" type="checkbox" /> 亚马逊贸易有限公司  
			  </label>  
			</li>  
			<li>  
			  <label> 4 
				<input name="company" value="4" type="checkbox" /> 脸书科技有限公司  
			  </label>  
			</li>  
			<li>  
			  <label> 5 
				<input name="company" value="5" type="checkbox" /> 谷歌网络有限公司  
			  </label>  
			</li>  
			<li>  
			  <label> 6 
				<input name="company" value="6" type="checkbox" /> 英伟达技术有限公司  
			  </label>  
			</li>  
			<li>  
			  <label> 7 
				<input name="company" value="7" type="checkbox" /> 台积电科技有限责任公司  
			  </label>  
			</li>  
			<li>  
			  <label> 8 
				<input name="company" value="8" type="checkbox" /> 沃尔玛股份有限公司  
			  </label>  
			</li>  
		</ul>  

        <input id="Button1" type="button" value="批量生成Word文件" onclick="ConvertFiles()"/><br/>

		<div id="progressDiv">
			单文件进度：
			<div class="progressBarContainer">
			  <div id="progressBar1" class="progressBar"></div>
			</div>
			整体进度：
			<div class="progressBarContainer">
			  <div id="progressBar2" class="progressBar"></div>
			</div>
			<div id="errorMsg"> </div>
		</div>
    </div>

</body>
</html>