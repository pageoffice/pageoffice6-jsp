<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.DocumentOpenType,com.zhuozhengsoft.pageoffice.FileMakerCtrl,com.zhuozhengsoft.pageoffice.word.WordDocumentWriter"
         pageEncoding="utf-8" %>
<%
	String[] companyArr = {"空", "微软（中国）有限公司", "IBM（中国）服务有限公司", "亚马逊贸易有限公司", "脸书科技有限公司", "谷歌网络有限公司", "英伟达技术有限公司", "台积电科技有限责任公司", "沃尔玛股份有限公司"};
	int id = Integer.parseInt(request.getParameter("id"));
    FileMakerCtrl fmCtrl = new FileMakerCtrl(request);
    WordDocumentWriter doc = new WordDocumentWriter();
    //给数据区域赋值，即把数据填充到模板中相应的位置
    doc.openDataRegion("PO_company").setValue(companyArr[id]);

    fmCtrl.setWriter(doc);
    fmCtrl.fillDocument("doc/template.doc", DocumentOpenType.Word);
	out.print(fmCtrl.getHtml());
%>

