<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    //打开ppt文档
    poCtrl.webOpen("doc/test.ppt", OpenModeType.pptNormalEdit, "张佚名");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
        pageofficectrl.AddCustomToolButton("保存", "Save", 1);
        pageofficectrl.AddCustomToolButton("关闭", "Close", 21);
    }

    function Save() {
		//设置保存文件方法
        pageofficectrl.SaveFilePage="/SimplePPT/SaveFile.jsp"
        pageofficectrl.WebSave();
    }

    function Close() {
        pageofficectrl.CloseWindow();
    }
</script>
<div style="height:750px;width:auto;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
