<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.excel.*" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%
    //设置PageOfficeCtrl控件的服务页面
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    //定义Workbook对象
    WorkbookWriter workBook = new WorkbookWriter();
    //定义Sheet对象，"Sheet1"是打开的Excel表单的名称
    SheetWriter sheet = workBook.openSheet("Sheet1");
    //定义Cell对象
    ExcelCellWriter cellB4 = sheet.openCell("B4");
    //给单元格赋值
    cellB4.setValue("1月");
    ExcelCellWriter cellC4 = sheet.openCell("C4");
    cellC4.setValue("300");
    ExcelCellWriter cellD4 = sheet.openCell("D4");
    cellD4.setValue("270");
    ExcelCellWriter cellE4 = sheet.openCell("E4");
    cellE4.setValue("270");
    ExcelCellWriter cellF4 = sheet.openCell("F4");
    DecimalFormat df = (DecimalFormat) NumberFormat.getInstance();
    cellF4.setValue(df.format(270.00 / 300 * 100) + "%");

    poCtrl.setWriter(workBook);
    //打开excel文件
    poCtrl.webOpen("doc/test.xls", OpenModeType.xlsNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>填充Excel表格</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数

        pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
    }

</script>
<div style="width: auto; height: 700px;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
