<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    //打开Word文档
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张佚名");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>控制保存、另存、打开和打印</title>
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数
        pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
    }
    function AfterDocumentOpened(){
        pageofficectrl.DisableSave=true;  //禁止保存
        pageofficectrl.DisableSaveAs=true; //禁止另存
        pageofficectrl.DisablePrint=true; //禁止打印
        pageofficectrl.DisableOpen=true;  //禁止打开

    }
</script>
<form id="form1">
    <div style=" width:auto; height:700px;">
        <%=poCtrl.getHtml()%>
    </div>
</form>
</body>
</html>
