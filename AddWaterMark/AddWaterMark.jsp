<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,com.zhuozhengsoft.pageoffice.word.WordDocumentWriter"
         pageEncoding="utf-8" %>
<%
    //******************************卓正PageOffice组件的使用*******************************
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    WordDocumentWriter doc = new WordDocumentWriter();
    //添加水印 ，设置水印的内容
    doc.getWaterMark().setText("PageOffice开发平台");
    poCtrl.setWriter(doc);
    //打开Word文档
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>添加水印</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数

        pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
    }
</script>
<!--**************   卓正 PageOffice组件 ************************-->
<div style=" width:auto; height:750px;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
