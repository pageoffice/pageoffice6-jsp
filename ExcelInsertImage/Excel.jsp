<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.excel.*" %>
<%
    WorkbookWriter workBook = new WorkbookWriter();
    SheetWriter sheet1 = workBook.openSheet("Sheet1");
    sheet1.openCell("A1").setValue("[image]/ExcelInsertImage/image/logo.jpg[/image]");
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    poCtrl.setWriter(workBook);//此行必须
	//打开excel文档
    poCtrl.webOpen("doc/test.xls", OpenModeType.xlsNormalEdit, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Excel中插入图片</title>
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数

        pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
    }

</script>
<form id="form1">
    <div style=" width:100%; height:700px;">
        <%=poCtrl.getHtml()%>
    </div>
</form>
</body>
</html>
