﻿<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.DocumentOpenType,com.zhuozhengsoft.pageoffice.FileMakerCtrl"
         pageEncoding="utf-8" %>
<%
    String id = request.getParameter("id").trim();
	String docName = "doc0" + id + ".doc";
	String pdfName = "doc0" + id + ".pdf";
	
    FileMakerCtrl fmCtrl = new FileMakerCtrl(request);

    fmCtrl.fillDocumentAsPDF("doc/" + docName, DocumentOpenType.Word, pdfName);
	out.print(fmCtrl.getHtml());
%>
