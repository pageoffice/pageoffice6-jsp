<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,javax.servlet.*,javax.servlet.http.*"
         pageEncoding="utf-8" %>
<%
    String id = request.getParameter("id").trim();
	String docName = "doc0" + id + ".doc";
	String filePath = request.getSession().getServletContext().getRealPath("FileMakerConvertPDFs/doc/" + docName);

    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    poCtrl.webOpen(filePath, OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>My JSP 'index.jsp' starting page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
	<script type="text/javascript">
		function Save() {
			//设置保存方法
            pageofficectrl.SaveFilePage="/FileMakerConvertPDFs/SaveFile.jsp"
			pageofficectrl.WebSave();
		}
		function OnPageOfficeCtrlInit() {
			pageofficectrl.AddCustomToolButton("保存", "Save()", 1);
		}
	</script>
</head>
<body>
	<div style="width:auto;height: 98vh;">
		<%=poCtrl.getHtml()%>
	</div>
</body>
</html>
