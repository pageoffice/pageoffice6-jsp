﻿<%@ page language="java" pageEncoding="utf-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <title></title>
	<style>
		table{
			border:solid 1px #ccc; 
			width: 600px;
			margin:20px;
		}
		th{
			border-bottom:
			solid 1px #ccc;
			text-align:left;
			padding: 5px;
		}
		td{
			padding: 5px;
		}
	</style>
	<style>
		.progressBarContainer {
		  width: 100%;
		  background-color: #eee;
		  border-radius: 5px;
		  padding: 3px;
		  box-shadow: 2px 2px 3px 3px #ccc inset;  
		}

		.progressBar {
		  height: 20px;
		  width: 0%;
		  background-color: #1A73E8;
		  border-radius: 5px;
		  text-align: center;
		  line-height: 20px; /* 使文字垂直居中 */
		  color: white;
		}
		
		#progressDiv{
			width:400px;
			margin: 10px auto;
			text-align: left;
			font-size:14px;
			border: solid 1px #1A73E8;
			padding:10px 20px;
			color: #1A73E8;
		}
		#errorMsg{
			color: red;
		}
	</style>
	<script type="text/javascript" src="../pageoffice.js"></script>
    <script type="text/javascript">
        var checkit = true;
        function selectall() {
            if (checkit) {
                var obj = document.all.check;
                for (var i = 0; i < obj.length; i++) {
                    obj[i].checked = true;
                    checkit = false;
                }
            } else {
                var obj = document.all.check;
                for (var i = 0; i < obj.length; i++) {
                    obj[i].checked = false;
                    checkit = true;
                }

            }

        }

		function ConvertFiles() {
			var ids = [];
			var checkboxes = document.getElementsByName('check');
			
			for (var i = 0; i < checkboxes.length; i++) {  
				if (checkboxes[i].checked) { 
					ids.push(checkboxes[i].value);  
				}  
			}  
			
			if(0 == ids.length){
				alert('请至少选择一个文档');
				return;
			}
			
			document.getElementById("Button1").disabled = true;
			ConvertFile(ids, 0);
		}
		
		function ConvertFile(idArr, index) {
            //设置保存方法
            filemakerctrl.SaveFilePage="/FileMakerConvertPDFs/SaveFile.jsp";
            //注意：CallFileMaker是pageoffice.js中封装好的方法，专门供调用FileMakerCtrl生成文件使用
            filemakerctrl.CallFileMaker({
				//url：指向服务器端FileMakerCtrl生成文件的controller地址
				url: "/FileMakerConvertPDFs/Convert.jsp?id="+idArr[index],
                success: function (res) {//res:获取服务器端fs.setCustomSaveResult设置的保存结果
					console.log(res);
                    console.log("completed successfully.");
                    setProgress1(100);
					
					index++;
					setProgress2(index, idArr.length);
	
					if(index < idArr.length){
						ConvertFile(idArr, index);
					} 
                },
                progress: function (pos) {
                    console.log("running "+pos+"%");
                    setProgress1(pos);
                },
                error: function (msg) {
					document.getElementById("errorMsg").innerHTML = "发生错误: <br /> " + msg;
                    console.log("error occurred: "+msg);
                }
            });
        }
		
		function setProgress1(percent) {
			var progressBar = document.getElementById("progressBar1");
			progressBar.style.width = percent + '%';
			progressBar.innerText = percent + '%';
		}
		
		function setProgress2(index, count) {
			var progressBar = document.getElementById("progressBar2");
			progressBar.style.width = Math.round(index/count*100) + '%';
			progressBar.innerText = index + '/' + count;
		}

    </script>
	
</head>
<body>

<div style="margin:100px" align="center">
	<h2>演示：批量转PDF</h2>
	<div style="width:600px;margin: 0 auto; font-size:14px;">
		<p style="text-align: left;">
			操作说明：<br/>
			1. 勾选下面的文件；<br/>
			2. 点击“批量转换PDF文档”按钮；<br/>
			3. 生成完毕后，即可在“FileMakerConvertPDFs/doc”目录下看到批量生成的PDF文件。<br/>
		</p>
	</div>
	<table id="table1" >
		<h3>文件列表</h3>
		<tr >
			<th><input name="checkAll" type="checkbox" onclick="selectall()"/></td>
			<th>序号</td>
			<th>文件名</td>
			<th>操作</td>
		</tr>
		<tr>
			<td><input name="check" type="checkbox" value="1"/></td>
			<td>01</td>
			<td>PageOffice产品简介</td>
			<td><a href="javascript:POBrowser.openWindow('Edit.jsp?id=1','width=1150px;height=800px;');">编辑</a></td>
		</tr>
		<tr>
			<td><input name="check" type="checkbox" value="2"/></td>
			<td>02</td>
			<td>PageOffice产品安装步骤</td>
			<td><a href="javascript:POBrowser.openWindow('Edit.jsp?id=2','width=1150px;height=800px;');">编辑</a></td>
		</tr>
		<tr>
			<td><input name="check" type="checkbox" value="3"/></td>
			<td>03</td>
			<td>PageOffice产品应用领域</td>
			<td><a href="javascript:POBrowser.openWindow('Edit.jsp?id=3','width=1150px;height=800px;');">编辑</a></td>
		</tr>
		<tr>
			<td><input name="check" type="checkbox" value="4"/></td>
			<td>04</td>
			<td>PageOffice产品对环境的要求</td>
			<td><a href="javascript:POBrowser.openWindow('Edit.jsp?id=4','width=1150px;height=800px;');">编辑</a></td>
		</tr>
	</table>

	<input type="button" id="Button1" value="批量转换PDF文档" onclick="ConvertFiles()"/>

	<div id="progressDiv">
		单文件进度：
		<div class="progressBarContainer">
		  <div id="progressBar1" class="progressBar"></div>
		</div>
		整体进度：
		<div class="progressBarContainer">
		  <div id="progressBar2" class="progressBar"></div>
		</div>
		<div id="errorMsg"> </div>
	</div>
</div>

</body>
</html>
