<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>

<%
    //******************************卓正PageOffice组件的使用*******************************
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    poCtrl.webOpen("doc/template.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>在word当前光标处插入书签</title>
</head>
<body>
<form id="form1">
    <div style=" font-size:small; color:Red;">
        <label>关键代码：点右键，选择“查看源文件”，看js函数：</label>
        <label>function addBookMark() 和 function delBookMark()</label>
        <br/>
        <label>插入书签时，请先输入要插入的书签名称和文本；删除书签时，请先输入相应的书签名称！</label><br/>
        <label>书签名称：</label><input id="txtBkName" type="text" value="test"/>
        &nbsp;&nbsp;<label>书签文本：</label><input id="txtBkText" type="text" value="[测试]"/>
    </div>
    <input id="Button1" type="button" onclick="addBookMark();" value="插入书签"/>
    <input id="Button2" type="button" onclick="delBookMark()" value="删除书签"/>
    <div style=" width:auto; height:700px;">
        <%=poCtrl.getHtml()%>
    </div>
</form>
<script type="text/javascript">

    function addBookMark() {
        var bkName = document.getElementById("txtBkName").value;
        var bkText = document.getElementById("txtBkText").value;
        pageofficectrl.word.AddDataRegion(bkName, bkText);
    }

    function delBookMark() {
        var bkName = document.getElementById("txtBkName").value;
        pageofficectrl.word.DeleteDataRegion(bkName);

    }
    function OnPageOfficeCtrlInit() {
        pageofficectrl.AddCustomToolButton("插入书签", "addBookMark", 5);
        pageofficectrl.AddCustomToolButton("删除书签", "delBookMark", 5);
    }
</script>
</body>
</html>
