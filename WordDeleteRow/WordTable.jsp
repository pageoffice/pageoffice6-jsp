<%@ page language="java" import="com.zhuozhengsoft.pageoffice.OpenModeType" pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.PageOfficeCtrl" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.*" %>
<%
    String FilePath = request.getContextPath() + "/WordDeleteRow/doc";
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    WordDocumentWriter doc = new WordDocumentWriter();
    WordTableWriter table1 = doc.openDataRegion("PO_table").openTable(1);
    WordCellWriter cell = table1.openCellRC(2, 1);
    //删除坐标为(2,1)的单元格所在行
    table1.removeRowAt(cell);

    poCtrl.setWriter(doc);
    poCtrl.webOpen("doc/test_table.doc", OpenModeType.docNormalEdit, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>删除Word中table中指定单元格所在行</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数

        pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
    }

</script>
<div style="color:Red">删除了table中坐标为(2,1)的单元格所在行，请在服务器<%=FilePath%>路径下查看原模板文档。</div>

<div style="width: auto; height: 600px;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
