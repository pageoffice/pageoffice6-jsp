<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.DocumentOpenType,com.zhuozhengsoft.pageoffice.FileMakerCtrl,com.zhuozhengsoft.pageoffice.word.WordDocumentWriter"
         pageEncoding="utf-8" %>
<%
    FileMakerCtrl fmCtrl = new FileMakerCtrl(request);
    WordDocumentWriter doc = new WordDocumentWriter();
    //给数据区域赋值，即把数据填充到模板中相应的位置
    doc.openDataRegion("PO_company").setValue("北京卓正志远软件有限公司");

    fmCtrl.setWriter(doc);
    fmCtrl.fillDocumentAsPDF("doc/template.doc", DocumentOpenType.Word, "zhengshu.pdf");
	out.print(fmCtrl.getHtml());
%>

