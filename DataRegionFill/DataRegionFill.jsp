<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,com.zhuozhengsoft.pageoffice.word.DataRegionWriter"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.WordDocumentWriter" %>
<%
    WordDocumentWriter doc = new WordDocumentWriter();
    //打开数据区域
    DataRegionWriter dataRegion1 = doc.openDataRegion("PO_userName");
    //给数据区域赋值
    dataRegion1.setValue("张三");
    DataRegionWriter dataRegion2 = doc.openDataRegion("PO_deptName");
    dataRegion2.setValue("销售部");

    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    poCtrl.setWriter(doc);
    //打开Word文件
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>简单的给Word文档中的数据区域赋值</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
</head>
<body>
	<script type="text/javascript">
		function OnPageOfficeCtrlInit() {
			// PageOffice的初始化事件回调函数
			pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
		}

	</script>

	<div style="width: auto; height: 700px;">
		<%=poCtrl.getHtml()%>
	</div>
</body>
</html>
