<%@ page language="java" import="com.zhuozhengsoft.pageoffice.OpenModeType" pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.PageOfficeCtrl" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.*" %>
<%
    String userName = request.getParameter("userName");
    //***************************卓正PageOffice组件的使用********************************
    WordDocumentWriter doc = new WordDocumentWriter();
    //打开数据区域
    DataRegionWriter dTitle = doc.openDataRegion("PO_title");
    //给数据区域赋值
    dTitle.setValue("某公司第二季度产量报表");
    //设置数据区域可编辑性
    dTitle.setEditing(false);//数据区域不可编辑
    DataRegionWriter dA1 = doc.openDataRegion("PO_A_pro1");
    DataRegionWriter dA2 = doc.openDataRegion("PO_A_pro2");
    DataRegionWriter dB1 = doc.openDataRegion("PO_B_pro1");
    DataRegionWriter dB2 = doc.openDataRegion("PO_B_pro2");
    //根据登录用户名设置数据区域可编辑性
    //A部门经理登录后
    if (userName.equals("zhangsan")) {
        userName = "A部门经理";
        dA1.setEditing(true);
        dA2.setEditing(true);
        dB1.setEditing(false);
        dB2.setEditing(false);
    }
    //B部门经理登录后
    else {
        userName = "B部门经理";
        dB1.setEditing(true);
        dB2.setEditing(true);
        dA1.setEditing(false);
        dA2.setEditing(false);
    }
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    poCtrl.setWriter(doc);


    //设置文档打开方式
    poCtrl.webOpen("doc/test.doc", OpenModeType.docSubmitForm, userName);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <title></title>
    <link href="images/csstg.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="content">
    <div id="textcontent" style="width: 1000px; height: 800px;">
        <div class="flow4">
            <input type="button" onclick="exit()" value="关闭窗口" />
            <strong>当前用户：</strong>
            <span style="color: Red;"><%=userName%></span>
        </div>
        <script type="text/javascript">
            function OnPageOfficeCtrlInit() {
                // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
                pageofficectrl.AddCustomToolButton("保存", "Save", 1);
                pageofficectrl.AddCustomToolButton("全屏/还原", "IsFullScreen", 4);
            }

            //保存
            function Save() {
				//设置保存方法
				pageofficectrl.SaveFilePage="/SetDrByUserWord/SaveFile.jsp";
                pageofficectrl.WebSave();
            }

            //全屏/还原
            function  IsFullScreen() {
                pageofficectrl.FullScreen = !pageofficectrl.FullScreen;
            }

            function exit(){
                pageofficectrl.CloseWindow();
            }
        </script>
        <!--**************   卓正 PageOffice组件 ************************-->
        <%=poCtrl.getHtml()%>
    </div>
</div>

</body>
</html>

