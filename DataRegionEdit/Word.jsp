<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.BorderStyleType,com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.ThemeType" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.WordDocumentWriter" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    WordDocumentWriter doc = new WordDocumentWriter();
    doc.getTemplate().defineDataRegion("PO_Guarantor", "[担保人]");
    doc.getTemplate().defineDataRegion("PO_SupplierAddress", "[供货单位地址]");
    doc.getTemplate().defineDataRegion("PO_BuyerAddress", "[购货单位地址]");
    doc.getTemplate().defineDataRegion("PO_No", "[合同编号]");
    doc.getTemplate().defineDataRegion("PO_GuarantorPhone", "[担保人手机]");
    doc.getTemplate().defineDataRegion("PO_ProductName", "[产品名称]");
    doc.getTemplate().defineDataRegion("PO_Buyer", "[购货单位]");
    doc.getTemplate().defineDataRegion("PO_Supplier", "[供货单位]");

    poCtrl.setWriter(doc); // 必须。


    poCtrl.webOpen("doc/test.docx", OpenModeType.docNormalEdit, "zhangsan");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title></title>
    <style>
        body {
            margin: 0;
            padding: 0;
            display: flex;
        }
        #left-container {
            width: 360px;
            display: flex;
            flex-direction: column;
            border-right: 2px solid #ccc;
            padding: 20px;
            overflow: auto;
            font-size: 12px;
            height: 90vh;
        }
        #right-container {
            flex: 1;
            padding: 0px;
            height: 95vh;
        }
        #podiv{
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #left-title{
            text-align: center;
            font-size: 16px;
            padding-bottom: 10px;
            margin-bottom: 10px;
            border-bottom: solid 1px #ccc;
        }

        .input-group {
            margin-bottom: 20px;
            display: flex;
            align-items: center;
        }
        input[type="text"] {
            width: 70%;
            padding: 10px;
            margin-top: 5px;
            box-sizing: border-box;
            border: 1px solid #ccc;
            border-radius: 5px;
            font-size: 12px;
            outline: none;
        }

        input[type="submit"] {
            width: 80px;
            padding: 10px;
            margin-top: 5px;
            margin-left: 10px;
            box-sizing: border-box;
            border: none;
            border-radius: 5px;
            background-color: #4E6EF2;
            color: white;
            font-size: 12px;
            outline: none;
            cursor: pointer;
        }
        /* 表格样式 */
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        th {
            position: sticky;
            top: 0;
            background-color: #f2f2f2;
            z-index: 1;
        }

        /* 容器样式 */
        .container {
            height: 300px;
            overflow: auto;
            border: solid 1px #ccc;
            scrollbar-width: thin;
            scrollbar-color: #888 #f2f2f2;
        }

        /* 滚动条样式 */
        .container::-webkit-scrollbar {
            width: 8px;
        }

        .container::-webkit-scrollbar-track {
            background: #f2f2f2;
        }

        .container::-webkit-scrollbar-thumb {
            background-color: #888;
            border-radius: 4px;
        }

        .container::-webkit-scrollbar-thumb:hover {
            background-color: #555;
        }
        .delete-button {
            padding: 6px 6px;
            border: none;
            border-radius: 5px;
            background-color: #f44336;
            color: white;
            font-size: 12px;
            cursor: pointer;
        }
        .delete-button:hover {
            background-color: #d32f2f;
        }

        .normal-button {
            padding: 6px 6px;
            border: none;
            border-radius: 5px;
            background-color: #4E7EFF;
            color: white;
            font-size: 12px;
            cursor: pointer;
        }
        .normal-button:hover {
            background-color: #4E6EF2;
        }

        .locate-button {
            padding: 6px 6px;
            border: none;
            border-radius: 5px;
            background-color: #0abb87;
            color: white;
            font-size: 12px;
            cursor: pointer;
        }
        .locate-button:hover {
            background-color: #0a9966;
        }
    </style>

    <script type="text/javascript">
        //控件中的一些常用方法都在这里调用，比如保存，打印等等
        function OnPageOfficeCtrlInit() {
            pageofficectrl.AddCustomToolButton("保存", "Save", 1);
        }
        function Save() {
			//设置保存方法
            pageofficectrl.SaveFilePage="/DataRegionEdit/SaveFile.jsp"
            pageofficectrl.WebSave();
        }
        //加载数据
        function loadData() {
            var kWord1 = document.getElementById("inputKey1").value;
            var kWord2 = document.getElementById("inputKey2").value;

            var definedDataRegionJson = pageofficectrl.word.DataRegionsDefinedAsJson;
            var dataRegionJson = pageofficectrl.word.DataRegionsAsJson;
            searchDataRegion(definedDataRegionJson, dataRegionJson, kWord1);
            searchDataRegion2(dataRegionJson, kWord2);
        }

        //加载上方数据列表
        function searchDataRegion(drDefinedJson, drJson, s){
            var tb1 = document.getElementById("bkmkTable");
            var rCount = tb1.rows.length;
            for (var i = 1; i < rCount; i++) {
                tb1.deleteRow(1);
            }

            if('' == drDefinedJson) drDefinedJson = '[]';
            let definedDataRegionObj = JSON.parse(drDefinedJson);
            let dataRegionsJson = drJson;
            if('' == dataRegionsJson) dataRegionsJson = '[]';
            let dataRegionsObj = JSON.parse(dataRegionsJson);

            var oTable = document.getElementById("bkmkTable");
            var tbodyObj = oTable.tBodies[0];
            for(let key in definedDataRegionObj ){
                let drName = definedDataRegionObj[key].name;
                let drCaption =  definedDataRegionObj[key].caption;
                //alert("数据区域："+drName+"; 值："+drValue);

                let bFind = false;
                for(let k in dataRegionsObj){
                    if(dataRegionsObj[k].name == drName){
                        bFind = true;
                        break;
                    }
                }

                if(bFind) continue;

                if (drName.toLocaleLowerCase().indexOf(s.toLocaleLowerCase()) > -1) {
                    var oTr = tbodyObj.insertRow();
                    var oTd = oTr.insertCell();
                    oTd.innerHTML = drName;
                    oTd = oTr.insertCell();
                    oTd.innerHTML = drCaption;
                    oTd = oTr.insertCell();
                    oTd.innerHTML = '<button class="normal-button" onclick="addDataRegion(\''+drName+'\',\''+drCaption+'\');loadData();">添加</button>';
                }
            }
        }

        //加载下方数据列表
        function searchDataRegion2(drJson, s) {
            //删除所有行
            var tb1 = document.getElementById("bkmkTable2");
            var rCount = tb1.rows.length;
            for (var i = 1; i < rCount; i++) {
                tb1.deleteRow(1);
            }

            let dataRegionsJson = drJson;
            if('' == dataRegionsJson) dataRegionsJson = '[]';
            let dataRegionsObj = JSON.parse(dataRegionsJson);
            var oTable = document.getElementById("bkmkTable2");
            var tbodyObj = oTable.tBodies[0];
            for(let key in dataRegionsObj ){
                let drName = dataRegionsObj[key].name;

                if (drName.toLocaleLowerCase().indexOf(s.toLocaleLowerCase()) > -1) {
                    var oTr = tbodyObj.insertRow();
                    var oTd = oTr.insertCell();
                    oTd.innerHTML = drName;
                    oTd = oTr.insertCell();
                    oTd.innerHTML = '<button class="delete-button" onclick="deleteDataRegion(\''+ drName +'\');loadData();">删除</button> <button class="locate-button" onclick="locateDataRegion(\''+ drName +'\');">定位</button>';
                }
            }

        }

        function locateDataRegion(drName) {
            pageofficectrl.word.LocateDataRegion(drName);
        }

        function deleteDataRegion(drName){
            pageofficectrl.word.DeleteDataRegion(drName);
        }

        function addDataRegion(drName, drValue){
            pageofficectrl.word.AddDataRegion(drName, drValue);
        }

        function AfterDocumentOpened() {
            loadData();
        }
    </script>
</head>
<body>
<div id="left-container">
    <div id="left-title">定义数据区域</div>
    <div class="input-group">
        <span style="font-size: 14px;">待添加区域：</span><input type="text" id="inputKey1" oninput="loadData();" placeholder="请输入数据区域关键字搜索">
    </div>
    <div class="container">
        <table id="bkmkTable">
            <thead>
            <tr>
                <th>数据区域</th>
                <th>显示文字</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <!-- 大量的数据行 -->
            <!--<tr>-->
            <!--<td>AAA</td>-->
            <!--<td>BBB</td>-->
            <!--<td><button class="normal-button">添加</button></td>-->
            <!--</tr>-->

            </tbody>
        </table>
    </div>
    <div class="input-group" style="margin-top: 20px">
        <span style="font-size: 14px;">已添加区域：</span><input type="text" id="inputKey2" oninput="loadData();" placeholder="请输入数据区域关键字搜索">
    </div>
    <div class="container">
        <table id="bkmkTable2">
            <thead>
            <tr>
                <th>数据区域</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <!-- 大量的数据行 -->
            <!--<tr>-->
            <!--<td>AAA</td>-->
            <!--<td>BBB</td>-->
            <!--<td><button class="delete-button">删除</button> <button class="normal-button">定位</button></td>-->
            <!--</tr>-->

            </tbody>
        </table>
    </div>
</div>
<div id="right-container">
    <div id="podiv">
        <%=poCtrl.getHtml()%>
    </div>
    <!-- 右侧内容 -->
</div>
</body>
</html>
