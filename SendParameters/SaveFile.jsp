﻿<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.FileSaver,java.sql.Connection,java.sql.DriverManager"
         pageEncoding="utf-8" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.net.URLDecoder" %>
<%
    FileSaver fs = new FileSaver(request, response);
    fs.saveToFile(request.getSession().getServletContext().getRealPath("SendParameters/doc") + "/" + fs.getFileName());
    int id = 0;
    String userName = "";
    int age = 0;
    String sex = "";

    //获取通过Url传递过来的值
    if (request.getParameter("id") != null
            && request.getParameter("id").trim().length() > 0)
        id = Integer.parseInt(request.getParameter("id").trim());

    //获取通过网页标签控件传递过来的参数值，注意fs.getFormField("参数名")方法中的参数名是值标签的Id

    //获取通过文本框<input type="text" />标签传递过来的值
    if (fs.getFormField("userName") != null
            && fs.getFormField("userName").trim().length() > 0) {
        userName = fs.getFormField("userName");
    }

    //获取通过隐藏域传递过来的值
    if (fs.getFormField("age") != null
            && fs.getFormField("age").trim().length() > 0) {
        age = Integer.parseInt(fs.getFormField("age"));
    }
    /**
     * 实际开发中，一般获取数据区域的值后用来和数据库进行交互，比如根据刚才获取的数据进行数据库记录的新增，更新，删除等。
     * 此处为了给用户展示获取的数据内容，通过setCustomSaveResult将获取的数据区域的值返回到前端页面给用户检查执行的结果。
     * 如果只是想返回一个保存结果，可以使用比如：setCustomSaveResult("ok")，前端可以根据这个保存结果进行下一步逻辑处理。
     */
    fs.setCustomSaveResult("id:"+id+"\r\nage:"+age+"\r\nuserName:"+userName);
    fs.close();
%>

