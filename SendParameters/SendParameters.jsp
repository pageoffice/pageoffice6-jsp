﻿<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl pocCtrl = new PageOfficeCtrl(request);

    //打开文件
    pocCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title></title>
    <script type="text/javascript">
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.AddCustomToolButton("保存", "Save", 1);
        }
        function Save() {
			//设置保存文件方法
            pageofficectrl.SaveFilePage="/SendParameters/SaveFile.jsp?id=1"
            //在这里写您保存前的代码
            pageofficectrl.WebSave();
            //在这里写您保存后的代码，比如判断保存结果pageofficectrl.CustomSaveResult
            alert(pageofficectrl.CustomSaveResult);
        }

    </script>
</head>
<body>
<form id="form1">
    <div style="font-size: 14px;">
        <div style="border:1px solid black;">PageOffice给保存页面传值的三种方式：</br>
            <span style="color: Red;">1.通过设置保存页面的url中的?给保存页面传递参数：</span></br>
            &nbsp;&nbsp;&nbsp;例:pageofficectrl.SaveFilePage="/SendParameters/SaveFile.jsp?id=1";</br>
            &nbsp;&nbsp;&nbsp;保存页面获取参数的方法：</br>
            &nbsp;&nbsp;&nbsp;String value=request.getParameter("id");</br>

            <span style="color: Red;">2.通过input隐藏域给保存页面传递参数：</span></br>
            &nbsp;&nbsp;&nbsp;例:
            <xmp><input id="age" name="age" type="hidden" value="25"/></xmp>
            </br>
            &nbsp;&nbsp;&nbsp;保存页面获取参数的方法：</br>
            &nbsp;&nbsp;&nbsp;String age=fs.getFormField("age");</br>
            &nbsp;&nbsp;&nbsp;注意：获取Form控件传递过来的参数值，fs.getFormField("参数名")方法中的参数名是当前控件的id，更多详细代码请查看SaveFile.jsp。<br>

            <span style="color: Red;">3.通过input隐藏域给保存页面传递参数：</span></br>
            &nbsp;&nbsp;&nbsp;例:
            <xmp><input id="userName" name="userName" type="text" value="zhangsan"/></xmp>
            </br>
            &nbsp;&nbsp;&nbsp;保存页面获取参数的方法：</br>
            &nbsp;&nbsp;&nbsp; String name=fs.getFormField("userName");</br>
            &nbsp;&nbsp;&nbsp;注意：获取Form控件传递过来的参数值，fs.getFormField("参数名")方法中的参数名是当前控件的id，更多详细代码请查看SaveFile.jsp。<br>

            <span style="color: Red;">更新人员信息：</span><br/>
            <input id="age" name="age" type="hidden" value="25"/>
            <span style="color: Red;">姓名：</span><input id="userName" name="userName" type="text" value="zhangsan"/><br/>

        </div>
    </div>
    <div style="width: auto; height: 700px;">
        <%=pocCtrl.getHtml()%>
    </div>
</form>
</body>
</html>
