<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.word.*"
         pageEncoding="utf-8" %>
<%
    WordDocumentReader doc = new WordDocumentReader(request, response);
    String companyName = doc.getFormField("txtCompany");
    //获取提交的数值
    DataRegionReader dataUserName = doc.openDataRegion("PO_userName");
    DataRegionReader dataDeptName = doc.openDataRegion("PO_deptName");
    /**
     * 实际开发中，一般获取数据区域的值后用来和数据库进行交互，比如根据刚才获取的数据进行数据库记录的新增，更新，删除等。
     * 此处为了给用户展示获取的数据内容，通过setCustomSaveResult将获取的数据区域的值返回到前端页面给用户检查执行的结果。
     * 如果只是想返回一个保存结果，可以使用比如：setCustomSaveResult("ok")，前端可以根据这个保存结果进行下一步逻辑处理。
     */
    doc.setCustomSaveResult("公司名称:"+companyName+"\r\n员工姓名:"+dataUserName.getValue()+"\r\n部门名称:"+dataDeptName.getValue());//通过setCustomSaveResult给前端页面返回数据
    doc.close();
%>
