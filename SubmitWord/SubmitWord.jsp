﻿<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,com.zhuozhengsoft.pageoffice.word.*"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    WordDocumentWriter wordDoc = new WordDocumentWriter();
    //打开数据区域，openDataRegion方法的参数代表Word文档中的书签名称
    DataRegionWriter dataRegion1 = wordDoc.openDataRegion("PO_userName");
    //设置DataRegion的可编辑性
    dataRegion1.setEditing(true);
    //为DataRegion赋值,此处的值可在页面中打开Word文档后自己进行修改
    dataRegion1.setValue("");

    DataRegionWriter dataRegion2 = wordDoc.openDataRegion("PO_deptName");
    dataRegion2.setEditing(true);
    dataRegion2.setValue("");

    poCtrl.setWriter(wordDoc);

    //打开Word文档
    poCtrl.webOpen("doc/test.doc", OpenModeType.docSubmitForm, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>最简单的提交Word中的数据</title>
    <script type="text/javascript">
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.AddCustomToolButton("保存", "Save", 1);
        }
        function Save() {
			//设置保存方法
            pageofficectrl.SaveDataPage="/SubmitWord/SaveData.jsp"
            pageofficectrl.WebSave();
            //在这里写您保存后的代码，比如判断保存结果pageofficectrl.CustomSaveResult
            alert(pageofficectrl.CustomSaveResult);
        }
    </script>

</head>
<body>
<form id="form1">
    <div>
        <span style="color: Red; font-size: 14px;">请输入公司名称、年龄、部门等信息后，单击工具栏上的保存按钮</span>
        <br/>
        <span style="color: Red; font-size: 14px;">请输入公司名称：</span>
        <input id="txtCompany" name="txtCompany" type="text"/>
        <br/>
    </div>
    <div style="width: auto; height: 700px;">
        <%=poCtrl.getHtml()%>
    </div>
</form>
</body>
</html>
