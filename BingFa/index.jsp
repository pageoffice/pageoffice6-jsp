<%@ page language="java" pageEncoding="utf-8"
	import="java.sql.*,java.io.*,javax.servlet.*,javax.servlet.http.*,java.text.SimpleDateFormat,java.util.Date"
%>
<%
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<link rel="stylesheet" href="css/style.css" type="text/css"></link>
		<link rel="stylesheet" href="css/style2.css" type="text/css"></link>
		<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
		<!--引入PageOffice的js  -->
        <script type="text/javascript" src="../pageoffice.js"></script>
		<title>在线演示示例</title>
		<script type="text/javascript">
			function onColor(dd){
				dd.style.backgroundColor = "lightyellow";
			}
			function offColor(dd){
				dd.style.backgroundColor="";
			}

			// 编辑文件
			function editFile(id){
				var user = document.getElementById("slt_user").value;
				// 检查当前文档是否有用户正在编辑
				$.ajax({
					url: 'detectCurrentEditor.jsp?id=' + id, 
					type: 'GET', 
					dataType: 'json', 
					success: function(data) {
						if(data.editor == user || data.editor == ''){
							POBrowser.openWindow('word.jsp?id='+id+'&user='+encodeURIComponent(user) , 'width=1200px;height=800px;');
						}else{
							alert('用户“'+data.editor+'”正在编辑此文档，请稍后重试，或点击“查看”只读打开。');
						}
					},
					error: function(xhr, status, error) {
						console.error('请求失败：', error);
					}
				});
			}
			
			// 查看文件
			function viewFile(id){
				var user = document.getElementById("slt_user").value;
				POBrowser.openWindow('word2.jsp?id='+id+'&user='+encodeURIComponent(user) , 'width=1200px;height=800px;');
			}
			
			// 选择登录用户
			function selectUser(){
				var user = document.getElementById("slt_user").value;
				if('' == user){
					alert('请先选择用户。');
					return;
				}
				document.getElementById("div_login").style.display = "none";
				document.getElementById("div_table").style.display = "block";
				var user = document.getElementById("slt_user").value;
				document.getElementById("span_user").innerHTML = "当前用户：" + user;
			}
		</script>
	</head>
	<body>
		<!--header-->


		<!--header end-->
		<!--content-->
		<div class="zz-content mc clearfix pd-28">
			<div class="demo mc">
				<h2 class="fs-18">
					文件并发控制功能
				</h2>
				<h3 class="fs-14" style="color:black;">
					演示说明:
				</h3>
				<p style="text-indent: 2em;">
					本示例演示了如何自己开发文件并发控制功能，实现防止两个用户同时打开编辑同一份文件，造成文件保存时内容互相覆盖的问题。
					（如果您在线编辑的文件在流转中都是单用户操作的，就无需开发和使用并发控制功能，仅仅在一个处理节点中需要两个或两个以上的用户同时编辑同一个文件的时候才需要参考本示例的并发控制功能。）
				</p>
				<h3 class="fs-14" style="color:black;">
				操作步骤：
				</h3>
				<p style="text-indent: 2em;">
					1. 选择一个用户，比如“<span style="color:black;font-weight:bold;">张三</span>”，登录后点击“编辑”按钮打开文件列表中的一个文件，比如“<span style="color:black;font-weight:bold;">产品简介</span>”；
				</p>
				<p style="text-indent: 2em;">
					2. 重新打开一个新的浏览器窗口访问本页面，模拟另一个用户“<span style="color:black;font-weight:bold;">李四</span>”登录，然后同样编辑打开“<span style="color:black;font-weight:bold;">产品简介</span>”文件，查看文件并发控制的效果（提示文档已被其他用户打开）。
				</p>
			</div>

			<div id="div_login" class="mc" style="justify-content: center;align-items: center;display: flex;padding:30px; width:698px;">
				<div class="login-container">
				  <h2>登录</h2>
				  <form action="/login" method="post">
					<div class="form-group">
					  <label for="username">用户名:</label>
					  <select id="slt_user" name="username">
					    <option value="" disabled selected>请选择用户</option>
						<option value="张三">张三</option>
						<option value="李四">李四</option>
					  </select>
					</div>
					<div class="form-group">
					  <label for="password">密码:</label>
					  <input type="password" id="password" disabled name="password" value="123456" placeholder="请输入密码">
					</div>
					<div class="form-group">
					  <button type="button" onclick="selectUser();">登录</button>
					</div>
				  </form>
				</div>			
			</div>

			<div id="div_table" class="zz-talbeBox mc" style="display:none;margin-top:30px;">
				<div id="span_user" style="font-size:22px;float:right;padding-top:5px;"></div>
				<h2 class="fs-20">
					共享文档列表
				</h2>
				<table >
					<thead>
						<tr>
							<th width="10%">
								类型
							</th>
							<th >
								文档名称
							</th>
							<th width="20%">
								操作
							</th>
						</tr>
					</thead>
					<tbody>
						<%
						 Class.forName("org.sqlite.JDBC");
						 String strUrl = "jdbc:sqlite:"
				            + this.getServletContext().getRealPath("BingFa/BingFa.db");
			             Connection conn=DriverManager.getConnection(strUrl);
			             Statement stmt = conn.createStatement();
						 ResultSet rs = stmt.executeQuery("select * from doc order by id desc");

							while (rs.next()) {
								String str0 = rs.getString(1);
								if (str0 != null && str0.length() > 0) {
						%>				
						<tr onmouseover='onColor(this);' onmouseout='offColor(this);'>
							<td>
								<img src='images/office1.png' />
							</td>
							<td><%=rs.getString(3)%></td>
							<td>
								<a class="button-edit" href="javascript:editFile(<%=rs.getString(1)%>);">编辑</a> 
								<a class="button-view"  href="javascript:viewFile(<%=rs.getString(1)%>);">查看</a> 
							</td>
						</tr>
						<%
								}
							}
							rs.close();
							stmt.close();
							conn.close();
						%>
					</tbody>
				</table>
			</div>
		</div>
		<!--content end-->
		<!--footer-->

		<!--footer end-->
	</body>
</html>
