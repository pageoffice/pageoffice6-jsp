﻿<%@ page language="java"
		 import="java.util.*,java.net.*,com.zhuozhengsoft.pageoffice.*,java.sql.*,java.io.*,javax.servlet.*,javax.servlet.http.*" 
         pageEncoding="utf-8" %>
<%
	String id = request.getParameter("id");
	String user = request.getParameter("user");
    
    String fileName = "";
	String subject = "";
    String userName = URLDecoder.decode(user, "UTF-8");;

	Class.forName("org.sqlite.JDBC");
	String strUrl = "jdbc:sqlite:" + this.getServletContext().getRealPath("BingFa/BingFa.db");
	Connection conn=DriverManager.getConnection(strUrl);
	Statement stmt = conn.createStatement();		
    ResultSet rs = stmt.executeQuery("select * from doc where id="+id);
	if(rs.next()){
		fileName = rs.getString(2);
		subject = rs.getString(3);
	}
	rs.close();
	stmt.close();
	
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
	poCtrl.setCaption("当前用户：" + userName + " | 模式：只读查看 | 文档名称："+ subject );
    poCtrl.webOpen("doc/" + fileName, OpenModeType.docReadOnly, userName);
%>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title><%=subject%></title>
    <style>
	*{padding:0; margin:0;}
	</style>
    <script type="text/javascript">
		function OnBeforeBrowserClosed(){
			// 此处可以执行窗口关闭前需要执行的业务逻辑代码
			pageofficectrl.CloseWindow(true);//必须。否则窗口不会关闭。
		}
		function OnPageOfficeCtrlInit() {
			// PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
			 pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
			 pageofficectrl.OfficeToolbars = false; //隐藏Office工具栏
		}
    </script>
</head>
<body>
	<div style="width:auto; height:100vh;">
		<%=poCtrl.getHtmlCode()%>
	</div> 
</body>
</html>
