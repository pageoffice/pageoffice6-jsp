﻿<%@ page language="java"
		 import="java.util.*,java.net.*,com.zhuozhengsoft.pageoffice.*,java.sql.*,java.io.*,javax.servlet.*,javax.servlet.http.*" 
         pageEncoding="utf-8" %>
<%
	String id = request.getParameter("id");
	String user = request.getParameter("user");
    
    String fileName = "";
	String subject = "";
    String userName = URLDecoder.decode(user, "UTF-8");;

	Class.forName("org.sqlite.JDBC");
	String strUrl = "jdbc:sqlite:" + this.getServletContext().getRealPath("BingFa/BingFa.db");
	Connection conn=DriverManager.getConnection(strUrl);
	Statement stmt = conn.createStatement();		
    ResultSet rs = stmt.executeQuery("select * from doc where id="+id);
	if(rs.next()){
		fileName = rs.getString(2);
		subject = rs.getString(3);
	}
	rs.close();
	stmt.close();
	
	// 文档表中的Editor字段用来记录打开当前文件的用户
	Statement stmt2 = conn.createStatement();
    stmt2.executeUpdate("Update doc set Editor='"+ userName +"' where id="+id);
	stmt2.close();
	conn.close();
	
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
	poCtrl.setCaption("当前用户：" + userName  + " | 模式：修订编辑 | 文档名称："+ subject );
    poCtrl.setSaveFilePage("savefile.jsp");
    poCtrl.webOpen("doc/" +fileName, OpenModeType.docRevisionOnly, userName);
%>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title><%=subject%></title>
	<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
    <style>
	*{padding:0; margin:0;}
	</style>
	<script type="text/javascript">
		function OnPageOfficeCtrlInit() {
			// PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
			pageofficectrl.AddCustomToolButton("保存", "Save", 1);
			pageofficectrl.AddCustomToolButton("另存为", "SaveAs", 12);
			pageofficectrl.AddCustomToolButton("页面设置", "PrintSet", 0);
			pageofficectrl.AddCustomToolButton("打印", "PrintFile", 6);
			pageofficectrl.AddCustomToolButton("全屏/还原", "IsFullScreen", 4);
			pageofficectrl.AddCustomToolButton("-", "", 0);
			pageofficectrl.AddCustomToolButton("关闭", "Close", 21);
		}

		function Save() {
			pageofficectrl.WebSave();
		}
		function SaveAs() {
			 pageofficectrl.ShowDialog(3);
		}

		function PrintSet() {
			pageofficectrl.ShowDialog(5);
		}

		function PrintFile() {
			pageofficectrl.ShowDialog(4);
		}

		function Close() {
			pageofficectrl.CloseWindow();
		}

		function IsFullScreen() {
			pageofficectrl.FullScreen = !pageofficectrl.FullScreen;
		}
		
		// 通知服务器端，用户已停止编辑文档
		function SendCloseMsg(){
			$.ajax({
				url: 'clearCurrentEditor.jsp?id=<%=id%>', 
				type: 'GET', 
				dataType: 'json', 
				success: function(data) {
					console.log('请求成功：', data);
				},
				error: function(xhr, status, error) {
					console.error('请求失败：', error);
				}
			});
		}
		
		function OnBeforeBrowserClosed() {
			// 此处可以执行窗口关闭前需要执行的业务逻辑代码
			if(pageofficectrl.IsDirty){
				if (confirm("提示：文档已被修改，是否继续关闭放弃保存 ？")) {
					SendCloseMsg();
					pageofficectrl.CloseWindow(true);//必须。否则窗口不会关闭。
				} 
			}else{
				SendCloseMsg();
				pageofficectrl.CloseWindow(true);//必须。否则窗口不会关闭。
			}
			
		}
	</script>
</head>
<body>
	<div style="width:auto; height:100vh;">
		<%=poCtrl.getHtmlCode()%>
	</div>
</body>
</html>
