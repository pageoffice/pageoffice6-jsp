<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.*" %>

<%
    //******************************卓正PageOffice组件的使用*******************************
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    WordDocumentWriter wordDoc = new WordDocumentWriter();
    //打开数据区域，openDataRegion方法的参数代表Word文档中的书签名称
    DataRegionWriter dataRegion1 = wordDoc.openDataRegion("PO_test1");
    dataRegion1.setSubmitAsFile(true);
    DataRegionWriter dataRegion2 = wordDoc.openDataRegion("PO_test2");
    dataRegion2.setSubmitAsFile(true);
    dataRegion2.setEditing(true);
    DataRegionWriter dataRegion3 = wordDoc.openDataRegion("PO_test3");
    dataRegion3.setSubmitAsFile(true);

    poCtrl.setWriter(wordDoc);
    //打开Word文件
    poCtrl.webOpen("doc/test.doc", OpenModeType.docSubmitForm, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title></title>
</head>
<body>
<form id="form1">
    <div style="width: auto; height: 700px;">
        <!-- *********************PageOffice组件客户端JS代码*************************** -->
        <script type="text/javascript">
            function OnPageOfficeCtrlInit() {
                // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
                pageofficectrl.AddCustomToolButton("保存", "Save", 1);
            }
            function Save() {
				//设置保存方法
				pageofficectrl.SaveDataPage="/SplitWord/SaveData.jsp";
                pageofficectrl.WebSave();
            }
        </script>
        <div style=" font-size:14px; line-height:20px;">演示说明：<br/>点击“保存”按钮，PageOffice会把文档中三个数据区域（PO_test1，PO_test2，PO_test3）中的内容保存为三个独立的子文件（new1.doc，new2.doc，new3.doc）到“Samples6/SplitWord/doc”
            目录下。
        </div>
        <div style="color: red;font-size:14px; line-height:20px;">
            Word拆分功能只有企业版支持，并且文档的打开模式必须是OpenModeType.docSubmitForm，需要设置数据区域的属性dataRegion1.setSubmitAsFile(true)
            。<br/><br/></div>

        <!-- *********************PageOffice组件的引用*************************** -->
        <%=poCtrl.getHtml()%>
    </div>
</form>
</body>
</html>
