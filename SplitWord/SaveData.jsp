<%@ page language="java" import="com.zhuozhengsoft.pageoffice.word.*,java.io.FileOutputStream"
         pageEncoding="utf-8" %>
<%
    String filePath = request.getSession().getServletContext().getRealPath("SplitWord/doc/") + "/";
    WordDocumentReader doc = new WordDocumentReader(request, response);
    byte[] bWord;

    DataRegionReader dr1 = doc.openDataRegion("PO_test1");
    bWord = dr1.getFileBytes();
    FileOutputStream fos1 = new FileOutputStream(filePath + "new1.doc");
    fos1.write(bWord);
    fos1.flush();
    fos1.close();

    DataRegionReader dr2 = doc.openDataRegion("PO_test2");
    bWord = dr2.getFileBytes();
    FileOutputStream fos2 = new FileOutputStream(filePath + "new2.doc");
    fos2.write(bWord);
    fos2.flush();
    fos2.close();

    DataRegionReader dr3 = doc.openDataRegion("PO_test3");
    bWord = dr3.getFileBytes();
    FileOutputStream fos3 = new FileOutputStream(filePath + "new3.doc");
    fos3.write(bWord);
    fos3.flush();
    fos3.close();

    doc.close();
%>

