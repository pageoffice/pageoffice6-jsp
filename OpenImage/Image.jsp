﻿<%@ page language="java" import="com.zhuozhengsoft.pageoffice.PDFCtrl" pageEncoding="utf-8" %>
<%

    PDFCtrl poCtrl1 = new PDFCtrl(request);
    poCtrl1.webOpen("doc/test.jpg");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <style>
        #main {
            width: 1030px;
            height: 900px;
            border: #83b3d9 2px solid;
            background: #f2f7fb;

        }
        #shut {
            width: 45px;
            height: 30px;
            float: right;
            margin-right: -1px;
        }
        #shut:hover {
        }
    </style>
</head>
<body style="overflow:hidden">

<!--**************   卓正 PageOffice 客户端代码开始    ************************-->
<script language="javascript" type="text/javascript">
    function OnPDFCtrlInit() {
        pdfctrl.AddCustomToolButton("打印", "PrintFile()", 6);
        pdfctrl.AddCustomToolButton("-", "", 0);
        pdfctrl.AddCustomToolButton("实际大小", "SetPageReal()", 16);
        pdfctrl.AddCustomToolButton("适合页面", "SetPageFit()", 17);
        pdfctrl.AddCustomToolButton("适合宽度", "SetPageWidth()", 18);
        pdfctrl.AddCustomToolButton("缩小", "ZoomOut()", 15);
        pdfctrl.AddCustomToolButton("放大", "ZoomIn()", 14);
        pdfctrl.AddCustomToolButton("-", "", 0);
        pdfctrl.AddCustomToolButton("首页", "FirstPage()", 8);
        pdfctrl.AddCustomToolButton("上一页", "PreviousPage()", 9);
        pdfctrl.AddCustomToolButton("下一页", "NextPage()", 10);
        pdfctrl.AddCustomToolButton("尾页", "LastPage()", 11);
        pdfctrl.AddCustomToolButton("-", "", 0);
        pdfctrl.AddCustomToolButton("向左旋转90度", "SetRotateLeft()", 12);
        pdfctrl.AddCustomToolButton("向右旋转90度", "SetRotateRight()", 13);
    }
    function PrintFile() {
        pdfctrl.ShowDialog(4);
    }
    function SwitchFullScreen() {
        pdfctrl.FullScreen = !pdfctrl.FullScreen;
    }
    function SetPageReal() {
        pdfctrl.SetPageFit(1);
    }
    function SetPageFit() {
        pdfctrl.SetPageFit(2);
    }
    function SetPageWidth() {
        pdfctrl.SetPageFit(3);
    }
    function ZoomIn() {
        pdfctrl.ZoomIn();
    }
    function ZoomOut() {
        pdfctrl.ZoomOut();
    }
    function FirstPage() {
        pdfctrl.GoToFirstPage();
    }
    function PreviousPage() {
        pdfctrl.GoToPreviousPage();
    }
    function NextPage() {
        pdfctrl.GoToNextPage();
    }
    function LastPage() {
        pdfctrl.GoToLastPage();
    }
    function SetRotateRight() {
        pdfctrl.RotateRight();
    }
    function SetRotateLeft() {
        pdfctrl.RotateLeft();
    }
</script>
<div id="main">
    <div id="content" style="height:850px;width:1028px;overflow-y:auto;">
        <%=poCtrl1.getHtml()%>
    </div>
</div>
</body>
</html>
