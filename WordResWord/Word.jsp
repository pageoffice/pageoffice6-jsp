<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.*" %>
<%
    //******************************卓正PageOffice组件的使用*******************************
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    WordDocumentWriter worddoc = new WordDocumentWriter();
    //先在要插入word文件的位置手动插入书签,书签必须以“PO_”为前缀
    //给DataRegion赋值,值的形式为："[word]word文件路径[/word]、[excel]excel文件路径[/excel]、[image]图片路径[/image]"
    DataRegionWriter data1 = worddoc.openDataRegion("PO_p1");
    data1.setValue("[word]/WordResWord/doc/1.doc[/word]");
    DataRegionWriter data2 = worddoc.openDataRegion("PO_p2");
    data2.setValue("[word]/WordResWord/doc/2.doc[/word]");
    DataRegionWriter data3 = worddoc.openDataRegion("PO_p3");
    data3.setValue("[word]/WordResWord/doc/3.doc[/word]");

    poCtrl.setWriter(worddoc);

    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张三");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>后台编程插入Word文件到数据区域</title>
</head>
<body>
    <script type="text/javascript">
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.CustomToolbar = false;
        }
    </script>
	<div style="font-size: 12px; line-height: 20px; border-bottom: dotted 1px #ccc; border-top: dotted 1px #ccc;
			padding: 5px;">
		关键代码：<span style="background-color: Yellow;"> <br/>DataRegionWriter dataRegion
				= worddoc.openDataRegion("PO_开头的书签名称");
				<br/>
				dataRegion.setValue("[word]/WordResWord/doc/1.doc[/word]");</span><br/>
	</div>
	<br/>
	<form id="form1">
		<div style="width: auto; height: 700px;">
			<!--**************   PageOffice 客户端代码开始    ************************-->
			<%=poCtrl.getHtml()%>
			<!--**************   PageOffice 客户端代码结束    ************************-->
		</div>
	</form>
</body>
</html>
