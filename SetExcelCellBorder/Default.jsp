<%@ page language="java" import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.excel.*,java.awt.*" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    WorkbookWriter wb = new WorkbookWriter();
    SheetWriter sheet = wb.openSheet("Sheet1");
    // 设置背景
    ExcelTableWriter backGroundTable = sheet.openTable("A1:P200");
    //设置表格边框样式
    backGroundTable.getBorder().setLineColor(Color.white);

    // 设置单元格边框样式
    ExcelBorder C4Border = sheet.openTable("C4:C4").getBorder();
    C4Border.setWeight(XlBorderWeight.xlThick);
    C4Border.setLineColor(Color.yellow);
    C4Border.setBorderType(XlBorderType.xlAllEdges);

    // 设置单元格边框样式
    ExcelBorder B6Border = sheet.openTable("B6:B6").getBorder();
    B6Border.setWeight(XlBorderWeight.xlHairline);
    B6Border.setLineColor(Color.magenta);
    B6Border.setLineStyle(XlBorderLineStyle.xlSlantDashDot);
    B6Border.setBorderType(XlBorderType.xlAllEdges);

    //设置表格边框样式
    ExcelTableWriter titleTable = sheet.openTable("B4:F5");
    titleTable.getBorder().setWeight(XlBorderWeight.xlThick);
    titleTable.getBorder().setLineColor(new Color(0, 128, 128));
    titleTable.getBorder().setBorderType(XlBorderType.xlAllEdges);

    //设置表格边框样式
    ExcelTableWriter bodyTable2 = sheet.openTable("B6:F15");
    bodyTable2.getBorder().setWeight(XlBorderWeight.xlThick);
    bodyTable2.getBorder().setLineColor(new Color(0, 128, 128));
    bodyTable2.getBorder().setBorderType(XlBorderType.xlAllEdges);

    poCtrl.setWriter(wb);
    //设置文档打开方式
    poCtrl.webOpen("doc/test.xls", OpenModeType.xlsNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <title></title>
    <link href="images/csstg.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <script type="text/javascript">
        //控件中的一些常用方法都在这里调用，比如保存，打印等等
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.CustomToolbar = false;
        }
    </script>
<div id="content">
    <div id="textcontent" style="width: 1000px; height: 800px;">
        <!--**************   卓正 PageOffice组件 ************************-->
        <%=poCtrl.getHtml()%>
    </div>
</div>
</body>
</html>
