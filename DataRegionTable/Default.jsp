<%@ page language="java" import="com.zhuozhengsoft.pageoffice.OpenModeType, com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@page import="com.zhuozhengsoft.pageoffice.word.*, java.awt.*" %>
<%
    //***************************卓正PageOffice组件的使用********************************
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

	WordDocumentWriter doc = new WordDocumentWriter();
	//打开数据区域
	DataRegionWriter dTable = doc.openDataRegion("PO_table");
	//设置数据区域可编辑性
	dTable.setEditing(true);

	//打开数据区域中的表格，OpenTable(index)方法中的index为word文档中表格的下标，从1开始
	WordTableWriter table1 = doc.openDataRegion("PO_Table").openTable(1);
	//设置表格边框样式
	table1.getBorder().setLineColor(Color.green);
	table1.getBorder().setLineWidth(WdLineWidth.wdLineWidth050pt);
	// 设置表头单元格文本居中
	table1.openCellRC(1, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 3).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(2, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(3, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);

	// 给表头单元格赋值
	table1.openCellRC(1, 2).setValue("产品1");
	table1.openCellRC(1, 3).setValue("产品2");
	table1.openCellRC(2, 1).setValue("A部门");
	table1.openCellRC(3, 1).setValue("B部门");

	poCtrl.setWriter(doc);
	//打开Word文档
	poCtrl.webOpen("/DataRegionTable/doc/test.doc", OpenModeType.docSubmitForm, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <title>数据区域提交表格</title>
    <link href="images/csstg.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="content">
    <div id="textcontent" style="width: 1000px; height: 800px;">
        <script type="text/javascript">
            function OnPageOfficeCtrlInit() {
                // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
                pageofficectrl.AddCustomToolButton("保存", "Save", 1);
            }
            //保存
            function Save() {
				//设置保存方法
				pageofficectrl.SaveDataPage="/DataRegionTable/SaveData.jsp"
                pageofficectrl.WebSave();
            }


        </script>
        <!--**************   卓正 PageOffice组件 ************************-->
        <%=poCtrl.getHtml()%>
    </div>
</div>
</body>
</html>
