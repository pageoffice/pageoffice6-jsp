<%@ page language="java" pageEncoding="utf-8" %>
<%
  
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title></title>
	<script type="text/javascript" src="../pageoffice.js"></script>
    <script type="text/javascript">
		var ids = []; 
		
		function PrintFiles() {
			var checkboxes = document.getElementsByName('company');
		
			for (var i = 0; i < checkboxes.length; i++) {  
				if (checkboxes[i].checked) { // 如果checkbox被选中  
					ids.push(checkboxes[i].value);  
				}  
			}  
			
			if(0 == ids.length){
				alert('请至少选择一个文件');
				return;
			}
			
			document.getElementById("Button1").disabled = true;
			PrintFile(ids, 0);
		}

        function PrintFile(idArr, index) {

			filemakerctrl.SetPrint();
			filemakerctrl.CallFileMaker({
                url: "/FileMakerPrintFiles/FileMaker.jsp?id="+idArr[index],
                success: function () {
                    console.log("completed successfully.");
                    setProgress1(100);
					
					index++;
					setProgress2(index, idArr.length);
	
					if(index < idArr.length){
						PrintFile(idArr, index);
					} 
                },
                progress: function (pos) {
                    console.log("running "+pos+"%");
                    setProgress1(pos);
                },
                error: function (msg) {
					document.getElementById("errorMsg").innerHTML = "发生错误: <br /> " + msg;
                    console.log("error occurred: "+msg);
                }
            });
        }
		
		function setProgress1(percent) {
			var progressBar = document.getElementById("progressBar1");
			progressBar.style.width = percent + '%';
			progressBar.innerText = percent + '%';
		}
		
		function setProgress2(index, count) {
			var progressBar = document.getElementById("progressBar2");
			progressBar.style.width = Math.round(index/count*100) + '%';
			progressBar.innerText = index + '/' + count;
		}
    </script>
	<style>
		.progressBarContainer {
		  width: 100%;
		  background-color: #eee;
		  border-radius: 5px;
		  padding: 3px;
		  box-shadow: 2px 2px 3px 3px #ccc inset;  
		}

		.progressBar {
		  height: 20px;
		  width: 0%;
		  background-color: #1A73E8;
		  border-radius: 5px;
		  text-align: center;
		  line-height: 20px; /* 使文字垂直居中 */
		  color: white;
		}
		
		#progressDiv{
			width:400px;
			margin: 10px auto;
			text-align: left;
			font-size:14px;
			border: solid 1px #1A73E8;
			padding:10px 20px;
			color: #1A73E8;
		}
		#errorMsg{
			color: red;
		}
	</style>
	<style>  
		/* 样式定义 */  
		.company-list {  
		  list-style-type: none;  
		  padding: 0;  
		  margin: 0 auto;  
		  width: 400px;
		}  
		  
		.company-list li {  
		  display: flex;  
		  justify-content: space-between;  
		  align-items: center;  
		  margin-bottom: 10px;  
		}  
		  
		.company-list label {  
		  display: block;  
		  font-weight: bold;  
		  margin-bottom: 5px;  
		}  
		  
		.company-list input[type="checkbox"] {  
		  margin-right: 5px;  
		}  
		
  </style>
</head>
<body>

    <div style="text-align: center; margin-top:30px;">
		<h3>演示：批量打印文件</h3>
		<div style="width:600px;margin: 0 auto; font-size:14px;">
			<p style="text-align: left;">
				演示内容：<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;本示例以Word为例，演示了批量打印文件的效果。选择文件后，点击“批量打印”按钮。
			</p>

		</div>
		
		<hr />
		<ul class="company-list">  
			<li>  
			  <label>  
				<input name="company" value="1" type="checkbox" /> 荣誉证书 
			  </label>
			  <a href="javascript:POBrowser.openWindow('Preview.jsp?id=1' , 'width=1200px;height=800px;');">预览</a>
			</li>  
			<li>  
			  <label> 
				<input name="company" value="2" type="checkbox" /> 公司工作总结大会的通知  
			  </label>  
			  <a href="javascript:POBrowser.openWindow('Preview.jsp?id=2' , 'width=1200px;height=800px;');">预览</a>
			</li>  
			<li>  
			  <label> 
				<input name="company" value="3" type="checkbox" /> 幻想科技销售合同 
			  </label>  
			  <a href="javascript:POBrowser.openWindow('Preview.jsp?id=3' , 'width=1200px;height=800px;');">预览</a>
			</li>  
			<li>  
			  <label> 
				<input name="company" value="4" type="checkbox" /> 某某科技公司公文  
			  </label>  
			  <a href="javascript:POBrowser.openWindow('Preview.jsp?id=4' , 'width=1200px;height=800px;');">预览</a>
			</li>  
		</ul>  

        <input id="Button1" type="button" value="批量打印" onclick="PrintFiles()"/><br/>

		<div id="progressDiv">
			单文件进度：
			<div class="progressBarContainer">
			  <div id="progressBar1" class="progressBar"></div>
			</div>
			整体进度：
			<div class="progressBarContainer">
			  <div id="progressBar2" class="progressBar"></div>
			</div>
			<div id="errorMsg"> </div>
		</div>
    </div>

</body>
</html>