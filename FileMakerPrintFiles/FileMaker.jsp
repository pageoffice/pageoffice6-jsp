<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.DocumentOpenType,com.zhuozhengsoft.pageoffice.FileMakerCtrl,com.zhuozhengsoft.pageoffice.wordwriter.WordDocument"
         pageEncoding="utf-8" %>
<%
	String id = request.getParameter("id");
	String fileName = "doc_" + id + ".doc";
    FileMakerCtrl fmCtrl = new FileMakerCtrl(request);
    fmCtrl.fillDocument("doc/" + fileName, DocumentOpenType.Word);
	out.print(fmCtrl.getHtml());
%>
