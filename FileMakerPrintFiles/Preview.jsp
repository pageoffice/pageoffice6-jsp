<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
	String id = request.getParameter("id");
	String fileName = "doc_" + id + ".doc";
	
    PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);
    poCtrl1.webOpen("doc/" + fileName, OpenModeType.docReadOnly, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>查看Word文档</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <script>
        function OnPageOfficeCtrlInit() {
            pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
            pageofficectrl.OfficeToolbars = false; //隐藏Office工具栏setTitlebar
			pageofficectrl.Caption = "文件预览（只读）";
        }
    </script>
</head>
<body style="margin:0; padding:0;">
<div style="width:auto; height:100vh;">
    <%=poCtrl1.getHtml()%>
</div>
</body>
</html>
