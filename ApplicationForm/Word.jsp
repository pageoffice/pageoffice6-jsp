<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,com.zhuozhengsoft.pageoffice.word.*,java.awt.*"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    //打开Word文档
    poCtrl.webOpen("doc/ShenQingBiao.docx", OpenModeType.docSubmitForm, "张佚名");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>DivDialog</title>
	<!-- 引入 layui.css -->
	<link href="layui/css/layui.css" rel="stylesheet">

	<script type="text/javascript">

		function Save() {
            pageofficectrl.SaveFilePage = "/ApplicationForm/SaveFile.jsp";
            pageofficectrl.WebSave();
		}
		function OnPageOfficeCtrlInit() {
			// PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
			pageofficectrl.OfficeToolbars = false;
            pageofficectrl.AddCustomToolButton("保存", "Save", 1);
            pageofficectrl.AddCustomToolButton("填写收入证明", "dialogOpen()", 0);
            pageofficectrl.AddCustomToolButton("填写个税申请表", "dialogOpen1()", 0);
		}

        function dialogOpen() {
            //定位光标到书签处
            pageofficectrl.word.LocateDataRegion("PO_zhengming");

            pageofficectrl.Enabled = false;
            var index = layer.open({
                type: 1, // page 层类型
                area: ['600px', '600px'],
                title: '收入证明-个人信息',
                shade: 0.6, // 遮罩透明度
                shadeClose: false, // 点击遮罩区域，关闭弹层
                anim: 0, // 0-6 的动画形式，-1 不开启
                content: `<div style="padding: 22px;">
					<form class="layui-form" action="">
					  <div class="layui-form-item">
						<label class="layui-form-label">姓名</label>
						<div class="layui-input-block">
						  <input type="text" name="unit_name" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
						</div>
					  </div>
                      <div class="layui-form-item">
                        <label class="layui-form-label">性别</label>
                        <div class="layui-input-block">
                          <input type="radio" name="gender" value="1" title="男" checked>
                          <input type="radio" name="gender" value="2" title="女">
                        </div>
                      </div>
					  <div class="layui-form-item">
						<label class="layui-form-label">身份证号码</label>
						<div class="layui-input-block">
						  <input type="text" name="unit_no" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
						</div>
					  </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">选择日期</label>
                            <div class="layui-input-block">
                                <input type="text" id="datePicker" name="startDate" class="layui-input" placeholder="请选择日期">
                            </div>
                        </div>
					  <div class="layui-form-item">
						<label class="layui-form-label">职务</label>
						<div class="layui-input-block">
						  <input type="text" placeholder="请输入内容" lay-verify="required" name="unit_position" autocomplete="off" class="layui-input">
						</div>
					  </div>
					  <div class="layui-form-item">
						<label class="layui-form-label">工资</label>
						<div class="layui-input-block">
						  <input type="text" placeholder="请输入内容" lay-verify="required" name="unit_salary" autocomplete="off" class="layui-input">
						</div>
					  </div>
					  <div class="layui-form-item">
						<div class="layui-input-block">
						  <button type="submit" class="layui-btn" lay-submit lay-filter="ok2">确定</button>
						  <button type="reset" class="layui-btn layui-btn-primary">重置</button>
						</div>
					  </div>
					</form>
				  </div>`,
                success: function(layero, index){
                    var btn = layero.find('.layui-btn[lay-filter="ok2"]');
                    btn.on('click', function(){
                        //layer.close(index);
                    });
                },
            });

            layui.use(['form', 'laydate'],function(){
                var form = layui.form;
                var laydate = layui.laydate;

                // 当表单元素被动态插入时，需主动进行组件渲染才能显示
                laydate.render({
                    elem: '#datePicker'
                });
                form.render(); // 渲染全部表单

                // 提交事件
                form.on('submit(ok2)', function(data){
                    var field = data.field; // 获取表单字段值

                    pageofficectrl.word.SetValueToDataRegion('PO_name', field.unit_name);
                    if('1' == field.gender){
                        pageofficectrl.word.SetValueToDataRegion('PO_sex', '男');
                    }else{
                        pageofficectrl.word.SetValueToDataRegion('PO_sex', '女');
                    }
                    pageofficectrl.word.SetValueToDataRegion('PO_idCard', field.unit_no);
                    pageofficectrl.word.SetValueToDataRegion('PO_startDate', field.startDate);//日期
                    pageofficectrl.word.SetValueToDataRegion('PO_job', field.unit_position);//职务
                    pageofficectrl.word.SetValueToDataRegion('PO_salary', field.unit_salary);//工资

                    layer.close(index);
                    return false; // 阻止默认 form 跳转
                });
            })
        }
		
		function dialogOpen1() {
            //定位光标到书签处
            pageofficectrl.word.LocateDataRegion("PO_shenqingbiao");

			pageofficectrl.Enabled = false;
			var index = layer.open({
			  type: 1, // page 层类型
			  area: ['600px', '600px'],
			  title: '纳税申请表-个人信息',
			  shade: 0.6, // 遮罩透明度
			  shadeClose: false, // 点击遮罩区域，关闭弹层
			  anim: 0, // 0-6 的动画形式，-1 不开启
			  content: `<div style="padding: 22px;">
						<form class="layui-form" action="">
						  <div class="layui-form-item">
							<label class="layui-form-label">姓名</label>
							<div class="layui-input-block">
							  <input type="text" name="person_name" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
							</div>
						  </div>
						  <div class="layui-form-item">
							<label class="layui-form-label">性别</label>
							<div class="layui-input-block">
							  <input type="radio" name="gender" value="1" title="男" checked>
							  <input type="radio" name="gender" value="2" title="女">
							</div>
						  </div>
						  <div class="layui-form-item">
							<div class="layui-inline">
							  <label class="layui-form-label">年龄</label>
							  <div class="layui-input-inline" style="width: 100px;">
								<input type="number" name="age" lay-verify="required" placeholder="" autocomplete="off" class="layui-input" min="0" step="1" lay-affix="number">
							  </div>
							</div>
						  </div>
						  <div class="layui-form-item">
							<label class="layui-form-label">国籍</label>
							<div class="layui-input-block">
							  <select name="nation" lay-verify="required" lay-filter="aihao">
								<option value="">请选择</option>
								<option value="中国">中国</option>
								<option value="美国">美国</option>
								<option value="加拿大">加拿大</option>
								<option value="澳大利亚">澳大利亚</option>
							  </select>
							</div>
						  </div>  
						  <div class="layui-form-item">
							<label class="layui-form-label">证件类别</label>
							<div class="layui-input-block">
							  <select name="id_type" lay-verify="required" lay-filter="aihao">
								<option value="">请选择</option>
								<option value="身份证">身份证</option>
								<option value="驾驶证">驾驶证</option>
								<option value="军官证">军官证</option>
							  </select>
							</div>
						  </div>  
						  <div class="layui-form-item">
							<label class="layui-form-label">证件号</label>
							<div class="layui-input-block">
							  <input type="text" name="id_no" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
							</div>
						  </div>
						  <div class="layui-form-item layui-form-text">
							<label class="layui-form-label">申请理由</label>
							<div class="layui-input-block">
							  <textarea placeholder="请输入内容" lay-verify="required" name="person_reason" class="layui-textarea"></textarea>
							</div>
						  </div>
						  <div class="layui-form-item">
							<div class="layui-input-block">
							  <button type="submit" class="layui-btn" lay-submit lay-filter="ok1">确定</button>
							  <button type="reset" class="layui-btn layui-btn-primary">重置</button>
							</div>
						  </div>
						</form>
					  </div>`,
			  success: function(layero, index){  
					var btn = layero.find('.layui-btn[lay-filter="ok1"]');   
					btn.on('click', function(){   
						//layer.close(index);  
					});  
			  }  
			});
			
			layui.use(function(){
				  var form = layui.form;
				  // 当表单元素被动态插入时，需主动进行组件渲染才能显示
				  form.render(); // 渲染全部表单
				  
				  // 提交事件
				  form.on('submit(ok1)', function(data){
					var field = data.field; // 获取表单字段值

					pageofficectrl.word.SetValueToDataRegion('PO_PersonName', field.person_name);
					if('1' == field.gender){
						pageofficectrl.word.SetValueToDataRegion('PO_PersonGender', '☑男 □女');
					}else{
						pageofficectrl.word.SetValueToDataRegion('PO_PersonGender', '□男 ☑女');
					}
					pageofficectrl.word.SetValueToDataRegion('PO_PersonAge', field.age);
					pageofficectrl.word.SetValueToDataRegion('PO_PersonNation', field.nation);
					pageofficectrl.word.SetValueToDataRegion('PO_PersonIDType', field.id_type);
					pageofficectrl.word.SetValueToDataRegion('PO_PersonID', field.id_no);
					pageofficectrl.word.SetValueToDataRegion('PO_PersonReason', field.person_reason);
					
					layer.close(index);
					return false; // 阻止默认 form 跳转
				  });
			});
			
		}
		

		

		
    </script>
</head>
<body>

<div style=" width:auto; height:99vh;">
    <%=poCtrl.getHtml()%>
</div>

<!-- 引入 layui.js -->
<script src="layui/layui.js"></script>
<script type="text/javascript">

function handlePageOfficeCtrlStatus(){
	let layShades = document.getElementsByClassName('layui-layer-shade');

	if(0 == layShades.length){ 
		let layNavChildren = document.getElementsByClassName('layui-nav-child');
		for (let i = 0; i < layNavChildren.length ; i++) {
			const attrClass = layNavChildren[i].getAttribute('class'); 
			if(attrClass.includes('layui-show')){
				pageofficectrl.Enabled = false;
				return;
			}
		}
		pageofficectrl.Enabled = true;  
	}else{
		pageofficectrl.Enabled = false; 
	}
	
}

function bodyBindListener(){
	document.addEventListener('mouseover', function(event) {  
		handlePageOfficeCtrlStatus()
	});
	
	document.addEventListener('mousemove', function(event) {  
		handlePageOfficeCtrlStatus()
	});
}


bodyBindListener();


</script>

</body>
</html>

