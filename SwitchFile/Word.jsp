﻿<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,com.zhuozhengsoft.pageoffice.word.*"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    String fileName = request.getParameter("fileName");
    //打开Word文档
    poCtrl.webOpen("doc/"+fileName, OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>POBrowser窗口中切换打开不同的文件</title>
    <style>
        /* 设置整个页面的样式 */
        html,
        body {
            height: 100%;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }

        .Word {
            display: flex;
        }

        .sidebar {
            flex: 0 0 20%;
            /* 启用Flex布局 */
            height: 100vh;
            /* 占满整个视口宽度（或省略，因为默认就是100%） */
            border-right: 1px solid #ccc;
            /* padding: 10px 100px */
            text-align: center;
            padding: 5px 20px;
        }


        .file-link {
            cursor: pointer;
            padding: 5px;
            border-bottom: 1px solid #eee;
            text-decoration: underline;
            color: #3568d7;
            /* Default link color */
            display: block;
            /* Make the link fill the entire li element */
        }

        .file-link:hover {
            background-color: #f0f0f0;
            /* Background color on hover */
            color: #42b983;
            /* Text color on hover */
        }

        .content {
            flex: 0 0 80%;
            padding: 5px;/* 内边距 */
        }
    </style>
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
        pageofficectrl.CustomToolbar = false; // 隐藏自定义工具栏
    }
    function selectFile(fileId) {
        var fileName = "test"+fileId+".docx"
        pageofficectrl.Caption = document.getElementById(fileId).textContent;//设置标题栏文件名
        document.getElementById("form1").action = "Word.jsp?fileName=" + fileName;
        document.forms[0].submit();
    }
</script>

<div class="Word">
    <div class="sidebar">
        <form method="post" id="form1">
            <h3>文件列表</h3>
            <ul>
                <li onclick="selectFile(1)" class="file-link" id="1" textContent ="PageOffice对客户端有什么要求">PageOffice对客户端有什么要求</li>
                <li onclick="selectFile(2)" class="file-link" id="2" textContent ="PageOffice授权协议">PageOffice授权协议</li>
                <li onclick="selectFile(3)" class="file-link" id="3" textContent ="试用版和正式版有什么区别">试用版和正式版有什么区别</li>
            </ul>
        </form>

    </div>
    <div class="content">
        <!-- 此div用来加载PageOffice客户端控件，其中div的高宽及位置就决定了控件的大小及位置 -->
        <div style="width:96%; height: 100%;">
            <%=poCtrl.getHtml()%>
        </div>
    </div>
</div>
</body>
</html>
