<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.WordDocumentWriter" %>
<%
    //******************************卓正PageOffice组件的使用*******************************
    //设置PageOffice服务器组件
	WordDocumentWriter doc=new WordDocumentWriter();
    doc.setDisableWindowSelection(true);//禁止选中
    doc.setDisableWindowRightClick(true);//禁止右键

    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    poCtrl.setWriter(doc);//注意：千万别忘了这句，否则WordDocument设置的所有方法都不生效
    poCtrl.setAllowCopy(false);//禁止拷贝，同时就实现了禁止F12另存功能了。
    //打开文件
    poCtrl.webOpen("doc/template.doc", OpenModeType.docReadOnly, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>演示：文件在线安全浏览</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
        pageofficectrl.CustomToolbar = false;
        pageofficectrl.OfficeToolbars = false;
    }
    function AfterDocumentOpened() {
        pageofficectrl.DisableSave=true;  //禁止保存
        pageofficectrl.DisableSaveAs=true; //禁止另存
        pageofficectrl.DisablePrint=true; //禁止打印
        pageofficectrl.DisableOpen=true;  //禁止打开
    }
</script>
<div style=" width:auto; height:700px;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
