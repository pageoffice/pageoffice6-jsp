﻿<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,com.zhuozhengsoft.pageoffice.word.*"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    WordDocumentWriter wordDoc = new WordDocumentWriter();
    //打开数据区域，openDataRegion方法的参数代表Word文档中的书签名称
    DataRegionWriter dataRegion1 = wordDoc.openDataRegion("PO_userName");
    //设置DataRegion的可编辑性
    dataRegion1.setEditing(true);
    DataRegionWriter dataRegion2 = wordDoc.openDataRegion("PO_deptName");
    dataRegion2.setEditing(true);
    poCtrl.setWriter(wordDoc);

    //打开Word文档,当需要同时保存数据和保存文档时,OpenModeType必须是docSubmitForm模式。
    poCtrl.webOpen("doc/test.doc", OpenModeType.docSubmitForm, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>保存文档中指定位置的数据的同时也保存该文档</title>
    <script type="text/javascript">
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.AddCustomToolButton("保存", "Save", 1);
        }
        function Save() {
            //同时保存数据和文件时的PageOffice的保存逻辑：默认先保存数据，再保存文件，如果保存数据失败了，则不保存文件；如果数据保存成功了，文件保存失败了，则正常返回保存数据成功的返回值
			//设置保存数据方法
            pageofficectrl.SaveDataPage="/SaveDataAndFile/SaveData.jsp"
			//设置保存文件方法
			pageofficectrl.SaveFilePage="/SaveDataAndFile/SaveFile.jsp"
            pageofficectrl.WebSave();

            //获取保存结果，根据保存结果进行下一步业务逻辑处理。保存数据和保存文件的返回值用\n分割。
            let saveResult = pageofficectrl.CustomSaveResult;
            let saveDataResult = saveResult.split("\n")[0];
            let saveFileResult = saveResult.split("\n")[1];
            alert("数据保存结果为：" + saveDataResult);
            alert("文件保存结果为：" + saveFileResult);
        }
    </script>

</head>
<body>
<form id="form1">
    <div>
        <span style="color: Red; font-size: 14px;">请输入公司名称、年龄、部门等信息后，单击工具栏上的保存按钮</span>
        <br/>
        <span style="color: Red; font-size: 14px;">请输入公司名称：</span>
        <input id="txtCompany" name="txtCompany" type="text"/>
        <br/>
    </div>
    <div style="width: auto; height: 700px;">
        <%=poCtrl.getHtml()%>
    </div>
</form>
</body>
</html>
