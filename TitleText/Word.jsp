<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    //打开Word文档
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>演示：修改PageOffice控件标题栏文本内容</title>
    <style>
        html, body {
            height: 100%;
        }

        .main {
            height: 100%;
        }
    </style>
    <script type="text/javascript">
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.Caption="PageOffice控件标题栏内容"
			pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏;
        }
    </script>
</head>
<body>
<form id="form1">
    <div style="font-size: 12px; line-height: 20px; border-bottom: dotted 1px #ccc; border-top: dotted 1px #ccc;
        padding: 5px;">
        操作：在前端PageOffice控件的初始化事件OnPageOfficeCtrlInit中调用修改标题栏的属性。<br/>
        关键代码：<span style="background-color:Yellow;"> pageofficectrl.Caption="PagficeCtrl控件标题栏内容";</span></div>
    <div style="height: 600px; width: auto;">
        <%=poCtrl.getHtml()%>
    </div>
</form>
</body>
</html>
