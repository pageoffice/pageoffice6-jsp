<%@ page language="java"
import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,com.zhuozhengsoft.pageoffice.excel.WorkbookWriter"	pageEncoding="utf-8"%>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    WorkbookWriter workBoook = new WorkbookWriter();
    workBoook.setDisableSheetRightClick(true);//禁止当前工作表鼠标右键

    poCtrl.setWriter(workBoook);
    //打开excel文档
    poCtrl.webOpen("doc/test.xls", OpenModeType.xlsNormalEdit, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>禁止Excel文档鼠标右键</title>
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数

        pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
    }

</script>
<form id="form1">
    <div style="color:Red">打开Excel文档后，鼠标右键，发现右键失效。</div>
    <div style=" width:100%; height:700px;">
        <%=poCtrl.getHtml()%>
    </div>
</form>
</body>
</html>
