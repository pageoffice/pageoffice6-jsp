<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,com.zhuozhengsoft.pageoffice.wordwriter.*,java.awt.*"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    //打开Word文档
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张佚名");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>DivDialog</title>
	<!-- 引入 layui.css -->
	<link href="layui/css/layui.css" rel="stylesheet">

	<script type="text/javascript">
		function ShowModalDlg() {
			pageofficectrl.Enabled = false;
			layer.open({
			  type: 1, // page 层类型
			  area: ['500px', '300px'],
			  title: '模态对话框',
			  shade: 0.6, // 遮罩透明度
			  shadeClose: false, // 点击遮罩区域，关闭弹层
			  maxmin: true, // 允许全屏最小化
			  anim: 0, // 0-6 的动画形式，-1 不开启
			  content: '<div style="padding: 32px;">自定义HTML字符串</div>'
			});
		}
		
		function OnPageOfficeCtrlInit() {
			// PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
			pageofficectrl.AddCustomToolButton("弹出模态窗口", "ShowModalDlg()", 0);
		}
		
    </script>
</head>
<body>

<ul class="layui-nav layui-bg-blue">
  <li class="layui-nav-item">
    <a href="javascript:;">菜单一</a>
    <dl class="layui-nav-child" >
      <dd><a href="" lay-on="msg">菜单1</a></dd>
      <dd><a href="" lay-on="prompt">菜单2</a></dd>
      <dd><a href="" lay-on="alert">菜单3</a></dd>
    </dl>
  </li>
  <li class="layui-nav-item">
    <a href="javascript:;" >菜单二</a>
    <dl class="layui-nav-child" >
      <dd><a href="" lay-on="msg2">选项1</a></dd>
      <dd class="layui-this"><a href="" lay-on="prompt2">选项2</a></dd>
	  <dd><a href="" lay-on="confirm2">选项3</a></dd>
    </dl>
  </li>
  <li class="layui-nav-item"><a href="" lay-on="confirm">询问框</a></li>
  <li class="layui-nav-item"><a href="" onclick="ShowModalDlg()">模态对话框</a></li>
</ul>
<div style=" width:auto; height:90vh;">
    <%=poCtrl.getHtml()%>
</div>

<!-- 引入 layui.js -->
<script src="layui/layui.js"></script>
<script>


layui.use(function(){
  var layer = layui.layer;
  var util = layui.util;
  // 批量事件
  util.on('lay-on', {
    alert: function(){
      layer.alert('对话框内容');
    },
    confirm: function(){
      layer.confirm('一个询问框的示例？', {
        btn: ['确定', '关闭'] //按钮
      }, function(){
        layer.msg('第一个回调', {icon: 1});
      }, function(){

      });
    },
	confirm2: function(){
		confirm('确认此操作吗？');	
		imgBindListener();
	},
	msg: function(){
      layer.msg('一段提示信息');
	  imgBindListener();
    },
	msg2: function(){
      alert('另一段提示信息');
	  imgBindListener();
    },
	prompt: function(){
      layer.prompt({title: '密令输入框', formType: 1}, function(pass, index){
        layer.close(index);
      });

    },
	prompt2: function(){
        layer.prompt({title: '文本输入框', formType: 2}, function(text, index){
          layer.close(index);
          alert('您输入的文本：'+ text);
        });
    
    },
  });
});


// 使用layui的use方法来使用layui的jquery模块
layui.use(['jquery'], function(){
  var $ = layui.jquery;

  // 为所有的a标签添加点击事件监听器
  $('a').on('click', function(event){
    // 阻止a标签的默认行为
    event.preventDefault();

    // 在这里可以添加你的代码，比如使用Ajax来加载内容而不刷新页面
    console.log('链接被点击，但页面不会重新加载');
  });
});
</script>

<script type="text/javascript">

function imgBindListener(){
	setTimeout(function() {
		// 获取img元素  
		var imgElement = document.getElementById('powebwps_div').querySelector('img');  
		  
		// 检查是否成功获取到img元素  
		if (imgElement) {  
			// 给img元素绑定click事件  
			imgElement.addEventListener('click', function(event) {  
				// 当img被点击时，这里的代码会被执行  
				console.log('Image was clicked!');  
				// 你可以在这里添加你想要执行的代码，比如改变图片源、显示弹窗等  
				pageofficectrl.Enabled = true;
			});
		}
	}, 500);
}

function handlePageOfficeCtrlStatus(){
	let layShades = document.getElementsByClassName('layui-layer-shade');

	if(0 == layShades.length){ 
		let layNavChildren = document.getElementsByClassName('layui-nav-child');
		for (let i = 0; i < layNavChildren.length ; i++) {
			const attrClass = layNavChildren[i].getAttribute('class'); 
			if(attrClass.includes('layui-show')){
				pageofficectrl.Enabled = false;
				return;
			}
		}
		pageofficectrl.Enabled = true;  
	}else{
		pageofficectrl.Enabled = false; 
	}
	
}

function bodyBindListener(){
	document.addEventListener('mouseover', function(event) {  
		handlePageOfficeCtrlStatus()
	});
	
	document.addEventListener('mousemove', function(event) {  
		handlePageOfficeCtrlStatus()
	});
}


bodyBindListener();

// 回调函数，当被观察节点发生变化时触发  
const callback = function(mutationsList, observer) {  
    // 遍历所有变化  
    for(let mutation of mutationsList) {  
        if (mutation.type === 'attributes') {  
			// 检查是否是 class 属性更改  
            if (mutation.attributeName === 'class') { 
				const newClass = mutation.target.getAttribute('class'); 
				console.log('The newClass: ' + newClass + ' .'); 
				console.log('The ' + mutation.attributeName + ' attribute was modified.');  
				if(newClass.includes('layui-show')){
					pageofficectrl.Enabled = false;
				}
				
			}
        }  
    }  
};  
  
// 创建一个新的观察器实例并传入回调函数  
const observer = new MutationObserver(callback);  
// 配置观察器选项  
const config = { attributes: true, attributeFilter: ['class'] };  

let layNavChildren = document.getElementsByClassName('layui-nav-child');
for (let i = 0; i < layNavChildren.length ; i++) {
   observer.observe(layNavChildren[i], config);
}


</script>

</body>
</html>

