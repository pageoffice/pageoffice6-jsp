<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.FileSaver"
         pageEncoding="utf-8" %>
<%
    FileSaver fs = new FileSaver(request, response);
    String fileName = "maker" + fs.getFileExtName();
    fs.saveToFile(request.getSession().getServletContext().getRealPath("FileMakerModify/doc") + "/" + fileName);
    //设置自定义保存结果，用来返回给前端页面，setCustomSaveResult的参数也可以是json字符串类型
    //设置文件的保存结果
    fs.setCustomSaveResult("ok");
    fs.close();
%>
