<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.DocumentOpenType,com.zhuozhengsoft.pageoffice.FileMakerCtrl,com.zhuozhengsoft.pageoffice.word.WordDocumentWriter,java.text.SimpleDateFormat,java.util.Date"
         pageEncoding="utf-8" %>
<%
    FileMakerCtrl fmCtrl = new FileMakerCtrl(request);
    WordDocumentWriter doc = new WordDocumentWriter();
    //修改数据区域“PO_Date”的值
    doc.openDataRegion("PO_Date").setValue(new SimpleDateFormat("yyyy年MM月dd日").format(new Date()).toString());
    fmCtrl.setWriter(doc);
    fmCtrl.fillDocument("doc/maker.doc", DocumentOpenType.Word);
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>My JSP 'FileMaker.jsp' starting page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
</head>
<body>
<div>
    <%=fmCtrl.getHtml()%>
</div>
</body>
</html>
