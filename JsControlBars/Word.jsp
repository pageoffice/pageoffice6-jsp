<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    //打开Word文档
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张佚名");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>用js控制工具栏的显示和隐藏</title>
</head>
<body>
<script type="text/javascript">
    // 隐藏/显示 标题栏
    function Button2_onclick() {
        var bVisible = pageofficectrl.Titlebar ;
        pageofficectrl.Titlebar  = !bVisible;
    }

    // 隐藏/显示 自定义工具栏
    function Button3_onclick() {
        var bVisible = pageofficectrl.CustomToolbar;
        pageofficectrl.CustomToolbar = !bVisible;
    }

    // 隐藏/显示 Office工具栏
    function Button4_onclick() {
        var bVisible = pageofficectrl.OfficeToolbars;
        pageofficectrl.OfficeToolbars = !bVisible;
    }
</script>
<form id="form1">
	<input id="Button2" type="button" value="隐藏/显示 标题栏" onclick="return Button2_onclick()"/>
    <input id="Button3" type="button" value="隐藏/显示 自定义工具栏" onclick="return Button3_onclick()"/>
    <input id="Button4" type="button" value="隐藏/显示 Office工具栏" onclick="return Button4_onclick()"/>
    <br/><br/>
    <div style=" width:auto; height:700px;">
        <%=poCtrl.getHtml()%>
    </div>
</form>
</body>
</html>
