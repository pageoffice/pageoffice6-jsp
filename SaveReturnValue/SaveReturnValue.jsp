<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    //******************************卓正PageOffice组件的使用*******************************
    //设置PageOffice服务器组件
    PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);

    //打开文件
    poCtrl1.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Word文档保存后获取返回值</title>
    <script type="text/javascript">
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.AddCustomToolButton("保存", "Save", 1);
            pageofficectrl.AddCustomToolButton("关闭", "Close", 21);
        }
        function Save() {
			//设置保存方法
            pageofficectrl.SaveFilePage="/SaveReturnValue/SaveFile.jsp"
            pageofficectrl.WebSave();
			alert("保存成功，返回值为：" + pageofficectrl.CustomSaveResult);
        }

        function Close() {
            pageofficectrl.CloseWindow();
        }
    </script>
</head>
<body>
<form id="form1">
    <div style=" font-size:small; color:Red;">
        <label>键代码：点右键，选择“查看源文件”，看js函数“Save()</label>
        <br/>pageofficectrl.WebSave()//执行保存操作"
        <br/>pageofficectrl.CustomSaveResult//获取返回值保存页面SaveFile.jsp代码fs.setCustomSaveResult("ok");定义的返回值
        <br/>
    </div>
    <div style=" width:auto; height:700px;">
        <%=poCtrl1.getHtml()%>
    </div>
</form>
</body>
</html>
