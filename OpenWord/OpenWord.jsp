<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    //******************************卓正PageOffice组件的使用*******************************
    //设置PageOffice服务器组件
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    //打开文件
    poCtrl.webOpen("doc/template.doc", OpenModeType.docReadOnly, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>最简单的以只读模式打开Word文档</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <script>
        function OnPageOfficeCtrlInit() {
            pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
            pageofficectrl.OfficeToolbars = false; //隐藏Office工具栏setTitlebar
            pageofficectrl.Caption="演示：最简单的以只读模式打开Word文档";
        }
    </script>
</head>
<body>
<div style=" width:auto; height:700px;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
