﻿<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.OfficeVendorType" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    //打开Word文档
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张佚名");
%>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>最简单在线打开文件</title>

</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
        pageofficectrl.AddCustomToolButton("保存", "Save", 1);
        pageofficectrl.AddCustomToolButton("另存为", "SaveAs", 12);
        pageofficectrl.AddCustomToolButton("页面设置", "PrintSet", 0);
        pageofficectrl.AddCustomToolButton("打印", "PrintFile", 6);
        pageofficectrl.AddCustomToolButton("全屏/还原", "IsFullScreen", 4);
        pageofficectrl.AddCustomToolButton("-", "", 0);
        pageofficectrl.AddCustomToolButton("关闭", "Close", 21);
    }

    function Save() {
		//设置保存文件方法
        pageofficectrl.SaveFilePage="/SimpleWord/SaveFile.jsp";
        //在这里写您保存前的代码
        pageofficectrl.WebSave();
        //在这里写您保存后的代码，比如判断保存结果pageofficectrl.CustomSaveResult
        if ("ok" == pageofficectrl.CustomSaveResult) {
            alert("保存成功！");
        } else {
            alert("保存失败！");
        }
    }
   function SaveAs() {
         pageofficectrl.ShowDialog(3);
    }

    function PrintSet() {
        pageofficectrl.ShowDialog(5);
    }

    function PrintFile() {
        pageofficectrl.ShowDialog(4);
    }

    function Close() {
        pageofficectrl.CloseWindow();
    }

    function IsFullScreen() {
        pageofficectrl.FullScreen = !pageofficectrl.FullScreen;
    }

</script>

    <div style=" width:auto; height:850px;">
        <%=poCtrl.getHtml()%>
    </div>
</body>
</html>
