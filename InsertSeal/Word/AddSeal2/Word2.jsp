﻿<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    //******************************卓正PageOffice组件的使用*******************************
    PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);

    poCtrl1.webOpen("word2.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title> 2.不弹出用户名、密码输入框盖章</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
        pageofficectrl.AddCustomToolButton("保存", "Save", 1);
        pageofficectrl.AddCustomToolButton("加盖印章", "InsertSeal()", 2);
    }
    //保存
    function Save() {
		//设置保存方法
        pageofficectrl.SaveFilePage="/InsertSeal/Word/AddSeal2/SaveFile.jsp"
        pageofficectrl.WebSave();
    }

    //加盖印章
    function InsertSeal() {
        try {
            pageofficectrl.zoomseal.AddSeal("李志");
        } catch (e) {
        }
    }
</script>
<div style="width: auto; height: 700px;">
    <%=poCtrl1.getHtml()%>
</div>
</body>

</html>
