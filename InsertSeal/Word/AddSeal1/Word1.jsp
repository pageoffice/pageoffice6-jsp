﻿<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    //******************************卓正PageOffice组件的使用*******************************
    PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);

    poCtrl1.webOpen("word1.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>1.常规盖章</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
</head>
<body>
<div style="font-size: 12px; line-height: 20px; border-bottom: dotted 1px #ccc; border-top: dotted 1px #ccc; padding: 5px;">
    <span style="color: red;">操作说明：</span>点“加盖印章”按钮即可，插入印章时的用户名为：李志，密码默认为：111111。
</div>
<br/>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
        pageofficectrl.AddCustomToolButton("保存", "Save", 1);
        pageofficectrl.AddCustomToolButton("加盖印章", "InsertSeal()", 2);
        if(("linux")!=(pageofficectrl.ClientOS)){
            pageofficectrl.AddCustomToolButton("根据印章名称删除印章", "DeleteByName()", 21);
            pageofficectrl.AddCustomToolButton("根据签章人删除印章", "DeleteBySigner", 21);
            pageofficectrl.AddCustomToolButton("验证印章", "VerifySeal()", 5);
            pageofficectrl.AddCustomToolButton("修改密码", "ChangePsw()", 0);
        }

    }
    //保存
    function Save() {
		//设置保存方法
        pageofficectrl.SaveFilePage="/InsertSeal/Word/AddSeal1/SaveFile.jsp"
        pageofficectrl.WebSave();
    }
    //加盖印章
    function InsertSeal() {
        try {
            pageofficectrl.zoomseal.AddSeal();
        } catch (e) {}
    }

    //验证印章
    function VerifySeal() {
        pageofficectrl.zoomseal.VerifySeal();
    }

    //修改密码
    function  ChangePsw() {
        pageofficectrl.zoomseal.ShowSettingsBox();
    }
    //根据印章名称删除印章（李志签名）
    function DeleteByName(){
        let sealsJson=pageofficectrl.zoomseal.SealsAsJson;
        let sealObj = JSON.parse(sealsJson);
        for(var key in sealObj ){
            let sealName=sealObj[key].name;
            if("李志签名"==sealName){
                pageofficectrl.zoomseal.DeleteByName(sealName);
            }
        }
    }
    //根据签章人删除印章（李志）
    function DeleteBySigner(){
        let sealsJson=pageofficectrl.zoomseal.SealsAsJson;
        let sealObj = JSON.parse(sealsJson);
        for(var key in sealObj ){
            let sealSigner=sealObj[key].signer;
            if("李志"==sealSigner){
                pageofficectrl.zoomseal.DeleteBySigner("李志");
            }
        }
    }
</script>
<div style="width: auto; height: 700px;">
    <%=poCtrl1.getHtml()%>
</div>
</body>
</html>
