﻿<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    poCtrl.webOpen("word4.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>4.编辑模版 - 在模版中添加盖章位置</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
	<script type="text/javascript">
		function OnPageOfficeCtrlInit(){
			pageofficectrl.AddCustomToolButton("保存", "Save()", 1);
			pageofficectrl.AddCustomToolButton("添加印章位置", "InsertSealPos()", 2);
		}
		
		function Save() {
            //设置保存方法
            pageofficectrl.SaveFilePage="/InsertSeal/Word/AddSeal4/SaveFile.jsp"
			pageofficectrl.WebSave();
		}

		function InsertSealPos() {
			try {
				pageofficectrl.zoomseal.AddSealPosition();
			} catch (e) {
			}
		}
	</script>
</head>
<body>
<div style="width: auto; height: 98vh;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>