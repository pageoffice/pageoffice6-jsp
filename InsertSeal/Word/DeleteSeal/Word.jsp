﻿<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="UTF-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    poCtrl.setSaveFilePage("SaveFile.jsp");
    poCtrl.webOpen("test.doc", OpenModeType.docNormalEdit, "李志");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>删除印章</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
	<script type="text/javascript">
		function OnPageOfficeCtrlInit(){
			pageofficectrl.AddCustomToolButton("保存", "Save", 1);
			pageofficectrl.AddCustomToolButton("加盖测试公章", "InsertSeal()", 2);
			pageofficectrl.AddCustomToolButton("加盖李志签名", "InsertSign()", 3);
			pageofficectrl.AddCustomToolButton("删除李志签名", "DeleteSign()", 21);
			pageofficectrl.AddCustomToolButton("删除所有李志的印章", "DeleteBySigner()", 21);
		}
		
		function Save() {
            //设置保存方法
            pageofficectrl.SaveFilePage="/InsertSeal/Word/DeleteSeal/SaveFile.jsp"
			pageofficectrl.WebSave();
		}

		function InsertSeal() {
			try {
				//第一个参数，必填项，标识印章名称（当存在重名的印章时，默认选取第一个印章），且该印章签章人的签章类型方式必须为用户名+密码；
				//第二个参数，可选项，标识是否保护文档，true:保护文档，false:不保护文档；
				//第三个参数，可选项，标识盖章指定位置名称，须为英文或数字，不区分大小写
				pageofficectrl.zoomseal.AddSealByName("测试公章", true);
			} catch (e) {
			}
		}
		
		function InsertSign(){
			try {
				//第一个参数，必填项，标识印章名称（当存在重名的印章时，默认选取第一个印章），且该印章签章人的签章类型方式必须为用户名+密码；
				//第二个参数，可选项，标识是否保护文档，true:保护文档，false:不保护文档；
				//第三个参数，可选项，标识盖章指定位置名称，须为英文或数字，不区分大小写
				pageofficectrl.zoomseal.AddSealByName("李志签名", true);
			} catch (e) {
			}
		}

		//根据印章名称删除印章（李志签名）
		function DeleteSign(){
			let sealsJson=pageofficectrl.zoomseal.SealsAsJson;
			let sealObj = JSON.parse(sealsJson);
			for(var key in sealObj ){
				let sealName=sealObj[key].name;
				if("李志签名"==sealName){
					pageofficectrl.zoomseal.DeleteByName(sealName);
				}
			}
		}
		//根据签章人删除印章（李志）
		function DeleteBySigner(){
			let sealsJson=pageofficectrl.zoomseal.SealsAsJson;
			let sealObj = JSON.parse(sealsJson);
			for(var key in sealObj ){
				let sealSigner=sealObj[key].signer;
				if("李志"==sealSigner){
					pageofficectrl.zoomseal.DeleteBySigner("李志");
				}
			}
		}
	</script>
</head>
<body>
<div style="width: auto; height: 98vh;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>