﻿<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    poCtrl.webOpen("word3.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>3.不弹出用户名、密码输入框并且不弹出印章选择框盖章</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
	<script type="text/javascript">
		function OnPageOfficeCtrlInit(){
			pageofficectrl.AddCustomToolButton("保存", "Save()", 1);
			pageofficectrl.AddCustomToolButton("加盖印章", "InsertSeal()", 2);
		}
		
		function Save() {
            //设置保存方法
            pageofficectrl.SaveFilePage="/InsertSeal/Word/AddSeal3/SaveFile.jsp"
			pageofficectrl.WebSave();
		}

		function InsertSeal() {
			try {
				/**
				 *第一个参数，必填项，标识印章名称（当存在重名的印章时，默认选取第一个印章）；
				 *第二个参数，可选项，标识是否保护文档，true：保护文档；false：不保护文档；
				 *第三个参数，可选项，标识盖章指定位置名称，须为英文或数字，不区分大小写
				 */
				var bRet = pageofficectrl.zoomseal.AddSealByName("测试公章", false, "");
				if (bRet) {
					alert("盖章成功！");
				} else {
					alert("盖章失败！");
				}
			} catch (e) {
			}
		}
	</script>	
</head>
<body>
<div style="width: auto; height: 98vh;">
    <%=poCtrl.getHtml()%>
</div>
</body>

</html>