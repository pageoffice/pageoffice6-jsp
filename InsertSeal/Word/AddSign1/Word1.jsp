﻿<%@ page language="java"
	import="java.util.*,com.zhuozhengsoft.pageoffice.*,com.zhuozhengsoft.pageoffice.wordwriter.*"
	pageEncoding="utf-8"%>
<%
	//******************************卓正PageOffice组件的使用*******************************
	PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);

	poCtrl1.webOpen("word1.doc", OpenModeType.docNormalEdit,"张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>1.常规签字</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
	</head>
	<body>
		<div style="font-size: 12px; line-height: 20px; border-bottom: dotted 1px #ccc; border-top: dotted 1px #ccc; padding: 5px;">
			<span style="color: red;">操作说明：</span>点“签字”按钮即可，插入签字时的用户名为：李志，密码默认为：111111。

		</div>
		<br />
			<script type="text/javascript">
			    function OnPageOfficeCtrlInit() {
                    // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
                    pageofficectrl.AddCustomToolButton("保存", "Save", 1);
                    pageofficectrl.AddCustomToolButton("签字", "InsertHandSign()", 2);
                    if(("linux")!=(pageofficectrl.ClientOS)){
                        pageofficectrl.AddCustomToolButton("验证印章", "VerifySeal()", 5);
                        pageofficectrl.AddCustomToolButton("修改密码", "ChangePsw()", 0);
                    }

                }
                function Save() {
					//设置保存方法
					pageofficectrl.SaveFilePage="/InsertSeal/Word/AddSign1/SaveFile.jsp"
                    pageofficectrl.WebSave();
                    alert("文件保存成功。");
                }

                function InsertHandSign() {
                    try {
                        pageofficectrl.zoomseal.AddHandSign();
                    } catch (e) {
                    }
                }

                function VerifySeal() {
                    pageofficectrl.zoomseal.VerifySeal();
                }

                function ChangePsw() {
                    pageofficectrl.zoomseal.ShowSettingsBox();
                }
			</script>
			<div style="width: auto; height: 700px;">
			    <%=poCtrl1.getHtml()%>
		    </div>
	</body>
</html>
