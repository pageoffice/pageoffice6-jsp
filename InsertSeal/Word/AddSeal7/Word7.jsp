﻿<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    poCtrl.webOpen("test.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>7.不弹出用户名、密码输入框并且不弹出印章选择框加盖印章到模板中的指定位置</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
	<script type="text/javascript">
		function OnPageOfficeCtrlInit(){
			pageofficectrl.AddCustomToolButton("保存", "Save()", 1);
			pageofficectrl.AddCustomToolButton("盖章到印章位置", "AddSealByPos()", 2);
		}

		function Save() {
            //设置保存方法
            pageofficectrl.SaveFilePage="/InsertSeal/Word/AddSeal7/SaveFile.jsp"
			pageofficectrl.WebSave();
		}

		function AddSealByPos() {
			try {
				//先定位到印章位置,再在印章位置上盖章
				pageofficectrl.zoomseal.LocateSealPosition("Seal1");
				/**
				 *第一个参数，必填项，标识印章名称（当存在重名的印章时，默认选取第一个印章）；
				 *第二个参数，可选项，标识是否保护文档，true：保护文档；false：不保护文档；
				 *第三个参数，可选项，标识盖章指定位置名称，须为英文或数字，不区分大小写
				 */
				var bRet = pageofficectrl.zoomseal.AddSealByName("李志签名", true, "Seal1"); //位置名称不区分大小写
				if (bRet) {
					alert("盖章成功！");
				} else {
					alert("盖章失败！");
				}
			} catch (e) {
			}
		}
	</script>
</head>
<body>
<div style="font-size: 12px; line-height: 20px; border-bottom: dotted 1px #ccc; border-top: dotted 1px #ccc; padding: 5px;">
    关键代码：点右键，选择“查看网页源代码”，看js函数<span style="background-color: Yellow;">AddSealByPos()</span>
</div>
<div style="width: auto; height: 90vh;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>