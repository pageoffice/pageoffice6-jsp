﻿<%@ page language="java"
	import="java.util.*,com.zhuozhengsoft.pageoffice.*,com.zhuozhengsoft.pageoffice.wordwriter.*"
	pageEncoding="UTF-8"%>
<%
	//******************************卓正PageOffice组件的使用*******************************
	PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

	poCtrl.webOpen("word9.doc", OpenModeType.docNormalEdit,"李志");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
	<title>9.加盖骑缝章</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript">
		function OnPageOfficeCtrlInit(){
			pageofficectrl.AddCustomToolButton("保存", "Save", 1);
			pageofficectrl.AddCustomToolButton("加盖骑缝章", "InsertSeal()", 2);
		}
		function Save() {
			//设置保存方法
			pageofficectrl.SaveFilePage="/InsertSeal/Word/AddSeal9/SaveFile.jsp"
			pageofficectrl.WebSave();
		}

		function InsertSeal() {
			try {						 
				pageofficectrl.zoomseal.AddSealQFZ();
			} catch(e) {}
		}
	</script>
</head>

<body>
<div style="font-size: 12px; line-height: 20px; border-bottom: dotted 1px #ccc; border-top: dotted 1px #ccc; padding: 5px;">
    <span style="color: red;">操作说明：</span>点“加盖骑缝章”按钮即可，插入印章时的用户名为：李志，密码默认为：111111或123456
</div>
<div style="width: auto; height: 93vh;">
    <%=poCtrl.getHtml()%>
</div>
</body>

</html>