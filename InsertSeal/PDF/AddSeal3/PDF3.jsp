﻿<%@ page language="java" import="com.zhuozhengsoft.pageoffice.PDFCtrl" pageEncoding="utf-8" %>
<%
    PDFCtrl pdfCtrl = new PDFCtrl(request);

    pdfCtrl.webOpen("hetong.pdf");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>3.加盖骑缝章</title>
<style>
	*{
		margin:0;
		padding:0;
	}
	div{
		box-sizing: border-box;  
	}
</style>
<script language="javascript" type="text/javascript">
    function Save() {
        //设置保存方法
        pdfctrl.SaveFilePage="/InsertSeal/PDF/AddSeal3/SaveFile.jsp"
        pdfctrl.WebSave();
    }

    function InsertSeal() {
        try {
            pdfctrl.zoomseal.AddSealQFZ();
        } catch (e) {
        }
    }
	
	function InsertSeal2() {
        try {
            pdfctrl.zoomseal.AddSealQFZ("李志");
        } catch (e) {
        }
    }
	
    function ChangePsw() {
        pdfctrl.zoomseal.ShowSettingsBox();
    }
    function AfterDocumentOpened() {
        //alert(pdfctrl.Caption);
    }
    function SetBookmarks() {
        pdfctrl.BookmarksVisible = pdfctrl.BookmarksVisible;
    }
    function PrintFile() {
        pdfctrl.ShowDialog(4);
    }
    function SwitchFullScreen() {
        pdfctrl.FullScreen = !pdfctrl.FullScreen;
    }
    function SetPageReal() {
        pdfctrl.SetPageFit(1);
    }
    function SetPageFit() {
        pdfctrl.SetPageFit(2);
    }
    function SetPageWidth() {
        pdfctrl.SetPageFit(3);
    }
    function ZoomIn() {
        pdfctrl.ZoomIn();
    }
    function ZoomOut() {
        pdfctrl.ZoomOut();
    }
    function FirstPage() {
        pdfctrl.GoToFirstPage();
    }
    function PreviousPage() {
        pdfctrl.GoToPreviousPage();
    }
    function NextPage() {
        pdfctrl.GoToNextPage();
    }
    function LastPage() {
        pdfctrl.GoToLastPage();
    }
    function SetRotateRight() {
        pdfctrl.RotateRight();
    }
    function SetRotateLeft() {
        pdfctrl.RotateLeft();
    }
    function OnPDFCtrlInit() {
		//PDF的初始化事件回调函数，您可以在这里添加自定义按钮
        pdfctrl.AddCustomToolButton("保存", "Save()", 6);
        pdfctrl.AddCustomToolButton("加盖骑缝章", "InsertSeal()", 2);
		pdfctrl.AddCustomToolButton("加盖骑缝章2", "InsertSeal2()", 2);
        pdfctrl.AddCustomToolButton("打印", "PrintFile()", 6);
        pdfctrl.AddCustomToolButton("-", "", 0);
        pdfctrl.AddCustomToolButton("实际大小", "SetPageReal()", 16);
        pdfctrl.AddCustomToolButton("适合页面", "SetPageFit()", 17);
        pdfctrl.AddCustomToolButton("适合宽度", "SetPageWidth()", 18);
        pdfctrl.AddCustomToolButton("缩小", "ZoomOut()", 17);
        pdfctrl.AddCustomToolButton("放大", "ZoomIn()", 18);
        pdfctrl.AddCustomToolButton("-", "", 0);
        pdfctrl.AddCustomToolButton("首页", "FirstPage()", 8);
        pdfctrl.AddCustomToolButton("上一页", "PreviousPage()", 9);
        pdfctrl.AddCustomToolButton("下一页", "NextPage()", 10);
        pdfctrl.AddCustomToolButton("尾页", "LastPage()", 11);
    }
</script>
</head>
<body>
<div style="height:100vh;width:auto;">
    <%=pdfCtrl.getHtml()%>
</div>
</body>
</html>
