﻿<%@ page language="java" import="com.zhuozhengsoft.pageoffice.PDFCtrl" pageEncoding="utf-8" %>
<%
    PDFCtrl pdfCtrl1 = new PDFCtrl(request);

    pdfCtrl1.webOpen("test.pdf");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
</head>
<body style="overflow:hidden">
<!--**************   卓正 PageOffice 客户端代码开始    ************************-->
<script language="javascript" type="text/javascript">
    function OnPDFCtrlInit() {
        pdfctrl.AddCustomToolButton("保存", "Save()", 6);
        pdfctrl.AddCustomToolButton("盖章", "InsertSeal()", 0);
        //PDF的初始化事件回调函数，您可以在这里添加自定义按钮
        if(("linux")!=(pdfctrl.ClientOS)){
            pdfctrl.AddCustomToolButton("修改密码", "ChangePsw()", 0);
            pdfctrl.AddCustomToolButton("隐藏/显示书签", "SetBookmarks()", 0);
        }
        pdfctrl.AddCustomToolButton("打印", "PrintFile()", 6);
        pdfctrl.AddCustomToolButton("-", "", 0);
        pdfctrl.AddCustomToolButton("实际大小", "SetPageReal()", 16);
        pdfctrl.AddCustomToolButton("适合页面", "SetPageFit()", 17);
        pdfctrl.AddCustomToolButton("适合宽度", "SetPageWidth()", 18);
        pdfctrl.AddCustomToolButton("缩小", "ZoomOut()", 17);
        pdfctrl.AddCustomToolButton("放大", "ZoomIn()", 18);
        pdfctrl.AddCustomToolButton("-", "", 0);
        pdfctrl.AddCustomToolButton("首页", "FirstPage()", 8);
        pdfctrl.AddCustomToolButton("上一页", "PreviousPage()", 9);
        pdfctrl.AddCustomToolButton("下一页", "NextPage()", 10);
        pdfctrl.AddCustomToolButton("尾页", "LastPage()", 11);
        pdfctrl.AddCustomToolButton("-", "", 0);
        pdfctrl.AddCustomToolButton("向左旋转90度", "SetRotateLeft()", 12);
        pdfctrl.AddCustomToolButton("向右旋转90度", "SetRotateRight()", 13);
    }
    function Save() {
		//设置保存方法
		pdfctrl.SaveFilePage="/InsertSeal/PDF/AddSeal1/SaveFile.jsp"
        pdfctrl.WebSave();
    }

    function InsertSeal() {
        try {
            pdfctrl.zoomseal.AddSeal();
        } catch (e) {
        }
    }
    //修改密码
    function ChangePsw() {
        pdfctrl.zoomseal.ShowSettingsBox();
    }
    function AfterDocumentOpened() {
        //alert(document.getElementById("PDFCtrl1").Caption);
    }
    function SetBookmarks() {
        pdfctrl.BookmarksVisible = pdfctrl.BookmarksVisible;
    }
    function PrintFile() {
        pdfctrl.ShowDialog(4);
    }
    function SwitchFullScreen() {
        pdfctrl.FullScreen = !pdfctrl.FullScreen;
    }
    function SetPageReal() {
        pdfctrl.SetPageFit(1);
    }
    function SetPageFit() {
        pdfctrl.SetPageFit(2);
    }
    function SetPageWidth() {
        pdfctrl.SetPageFit(3);
    }
    function ZoomIn() {
        pdfctrl.ZoomIn();
    }
    function ZoomOut() {
        pdfctrl.ZoomOut();
    }
    function FirstPage() {
        pdfctrl.GoToFirstPage();
    }
    function PreviousPage() {
        pdfctrl.GoToPreviousPage();
    }
    function NextPage() {
        pdfctrl.GoToNextPage();
    }
    function LastPage() {
        pdfctrl.GoToLastPage();
    }
    function SetRotateRight() {
        pdfctrl.RotateRight();
    }
    function SetRotateLeft() {
        pdfctrl.RotateLeft();
    }

</script>
<div style="height:850px;width:auto;">
    <%=pdfCtrl1.getHtml()%>
</div>
</body>
</html>
