<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl"
         pageEncoding="utf-8" %>
<%
    //******************************卓正PageOffice组件的使用*******************************
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    String id = request.getParameter("id");

    //打开Word文件
    poCtrl.webOpen("Openfile.jsp?id=" + id, OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>My JSP 'Edit.jsp' starting page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
</head>
<body>
<a href="#" onclick="window.external.close();">返回列表页</a>
<div style="width: auto; height: 700px;">
    <!-- *************************PageOffice组件的使用******************************** -->
    <script type="text/javascript">
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.AddCustomToolButton("保存", "Save", 1);
        }
        function Save() {
			//获取POBrowser.openWindow第三个参数，文件id
			var id = pageofficectrl.WindowParams;
			//设置保存方法
			pageofficectrl.SaveFilePage="/ExaminationPaper/SaveFile.jsp?id="+id;
            pageofficectrl.WebSave();
        }
    </script>
    <%=poCtrl.getHtml()%>
    <!-- *************************PageOffice组件的使用******************************** -->
</div>
</body>
</html>
