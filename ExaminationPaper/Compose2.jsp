<%@ page language="java"
	import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,com.zhuozhengsoft.pageoffice.word.*,javax.servlet.*" pageEncoding="utf-8" %>
<%@ page import="javax.servlet.http.*" %>
<%
    if (request.getParameter("ids").equals(null)
            || request.getParameter("ids").equals("")) {
        return;
    }
    String idlist = request.getParameter("ids").trim();
    String[] ids = idlist.split(","); //将idlist按照","截取后存到ids数组中，然后遍历数组用js插入文件
    String temp = "PO_begin";//存储数据区域名称
    int num = 1;//试题编号
    WordDocumentWriter doc = new WordDocumentWriter();
    for (int i = 0; i < ids.length; i++) {

        DataRegionWriter dataNum = doc.createDataRegion("PO_" + num,
                DataRegionInsertType.After, temp);
        dataNum.setValue(num + ".\t");
        DataRegionWriter dataRegion = doc.createDataRegion("PO_begin"
                + (i + 1), DataRegionInsertType.After, "PO_" + num);
        dataRegion.setValue("[word]/ExaminationPaper/Openfile.jsp?id=" + ids[i]
                + "[/word]");
        temp = "PO_begin" + (i + 1);
        num++;
    }

    //******************************卓正PageOffice组件的使用*******************************
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);

    poCtrl.setWriter(doc);
    //打开Word文件
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

    <title>在Word文档中动态生成 试卷</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
</head>
<body>
<script type="text/javascript">
    function OnPageOfficeCtrlInit() {
        // PageOffice的初始化事件回调函数
        pageofficectrl.CustomToolbar = false; //隐藏自定义工具栏
    }

</script>

<div style="width: auto; height: 700px;">
    <%=poCtrl.getHtml()%>
</div>
</body>
</html>
