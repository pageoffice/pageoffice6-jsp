<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.OpenModeType,com.zhuozhengsoft.pageoffice.PageOfficeCtrl,com.zhuozhengsoft.pageoffice.word.DataRegionWriter"
         pageEncoding="utf-8" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.DataRegionInsertType" %>
<%@ page import="com.zhuozhengsoft.pageoffice.word.WordDocumentWriter" %>
<%
    PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
    WordDocumentWriter doc = new WordDocumentWriter();
    //创建数据区域，createDataRegion 方法中的三个参数分别代表“新建的数据区域名称”，“数据区域将要插入的位置”，
    //“与新建的数据区域相关联的数据区域名称”，若当前Word文档中尚无数据区域（书签）或者想在文档的最开头创建时，那么第三个参数为“[home]”
    //若想在文档的结尾处创建数据区域则第三个参数为“[end]”
    DataRegionWriter dataRegion1 = doc.createDataRegion("reg1", DataRegionInsertType.After, "[home]");
    //设置创建的数据区域的可编辑性
    dataRegion1.setEditing(true);
    //给数据区域赋值
    dataRegion1.setValue("第一个数据区域\r\n");
    DataRegionWriter dataRegion2 = doc.createDataRegion("reg2", DataRegionInsertType.After, "reg1");
    dataRegion2.setEditing(true);
    dataRegion2.setValue("第二个数据区域");

    poCtrl.setWriter(doc);

    //打开Word文件
    poCtrl.webOpen("doc/test.doc", OpenModeType.docNormalEdit, "张三");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>新建数据区域</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">

</head>
<body>
    <script type="text/javascript">
        function OnPageOfficeCtrlInit() {
            // PageOffice的初始化事件回调函数，您可以在这里添加自定义按钮
            pageofficectrl.CustomToolbar = false;
        }
    </script>
	<div style="width: auto; height: 700px;">
		<%=poCtrl.getHtml()%>
	</div>
</body>
</html>
